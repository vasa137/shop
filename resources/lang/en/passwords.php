<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Dužina lozinke mora biti najmanje 6 karaktera.',
    'reset' => 'Uspešno ste resetovali lozinku!',
    'sent' => 'Poslat Vam je email sa linkom za reset lozinke!',
    'token' => 'Pogrešan link za reset lozinke.',
    'user' => "Ne postoji korisnik sa datom email adresom.",

];
