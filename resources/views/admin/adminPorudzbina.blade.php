@extends('admin.adminLayout')

@section('title')
Porudžbina - #{{$porudzbina->id}}
@stop

@section('breadcrumbs')
<a class="breadcrumb-item" href="/admin">Admin</a>
<a class="breadcrumb-item" href="/admin/porudzbine">Porudžbine</a>
<span class="breadcrumb-item active">Porudžbina - #{{$porudzbina->id}}</span>
@stop

@section('heder-h1')
Porudžbina - #{{$porudzbina->id}}
@stop


@section('scriptsTop')
<link href="{{asset('css/adminPorudzbina.css')}}" rel="stylesheet"/>
@endsection

@section('main')
<!-- Progress -->
<div class="row gutters-tiny">
    
    <!-- Payment -->
    <div class="col-md-3 col-xl-3">
        <div class="block block-rounded">
            <div class="block-content block-content-full">
                <div class="py-20 text-center">
                    @if($porudzbina->nacin_placanja == 'pouzecem')
                        <div class="mb-20">
                            <i class="fa fa-euro fa-3x text-info"></i>
                        </div>
                        <div class="font-size-sm font-w600 text-uppercase text-info"> Pouzećem</div>
                    @elseif($porudzbina->nacin_placanja == 'karticom')
                        <div class="mb-20">
                            <i class="fa fa-credit-card fa-3x text-info"></i>
                        </div>
                        <div class="font-size-sm font-w600 text-uppercase text-info"> Karticom</div>
                    @elseif($porudzbina->nacin_placanja == 'preko_racuna')
                        <div class="mb-20">
                            <i class="fa fa-file-text fa-3x text-info"></i>
                        </div>
                        <div class="font-size-sm font-w600 text-uppercase text-info"> Preko računa</div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <!-- END Payment -->

    <!-- Packaging -->
    <div class="col-md-3 col-xl-3">
        <div class="block block-rounded">
            <div class="block-content block-content-full">
                <div class="py-20 text-center">
                    @if($porudzbina->status == 'nova')
                        <div class="mb-20">
                            <i class="fa fa-flag  fa-3x text-warning"></i>
                        </div>
                        <div class="font-size-sm font-w600 text-uppercase text-warning"> Nova </div>
                    @elseif($porudzbina->status == 'poslata')
                        <div class="mb-20">
                            <i class="fa fa-spinner fa-3x fa-spin text-primary"></i>
                        </div>
                        <div class="font-size-sm font-w600 text-uppercase text-primary"> Poslata </div>
                    @elseif($porudzbina->status == 'stornirana')
                        <div class="mb-20">
                            <i class="fa fa-times fa-3x text-danger"></i>
                        </div>
                        <div class="font-size-sm font-w600 text-uppercase text-danger"> Stornirana </div>
                    @elseif($porudzbina->status == 'kompletirana')
                        <div class="mb-20">
                            <i class="fa fa-check fa-3x text-success"></i>
                        </div>
                        <div class="font-size-sm font-w600 text-uppercase text-success"> Kompletirana </div>
                    @endif

                </div>
            </div>
        </div>
    </div>
    <!-- END Packaging -->
    <div class="col-md-3 col-xl-3">
        <div class="block block-rounded">
            <div class="block-content block-content-full">
                <div class="py-20 text-center">
                    @if($porudzbina->placena == 0)
                        <div class="mb-20">
                            <i class="fa fa-times  fa-3x text-muted"></i>
                        </div>
                        <div class="font-size-sm font-w600 text-uppercase text-muted"> Nije plaćena </div>
                    @elseif($porudzbina->placena == 1)
                        <div class="mb-20">
                            <i class="fa fa-money fa-3x text-primary"></i>
                        </div>
                        <div class="font-size-sm font-w600 text-uppercase text-primary"> Plaćena </div>
                    @endif

                </div>
            </div>
        </div>
    </div>
    <!-- Completed -->
    <div class="col-md-3 col-xl-3">
        <a class="block block-rounded block-link-shadow" href="/admin/faktura/{{$porudzbina->id}}">
            <div class="block-content block-content-full">
                <div class="py-20 text-center">
                    <div class="mb-20">
                        <i class="fa fa-file fa-3x text-muted"></i>
                    </div>
                    <div class="font-size-sm font-w600 text-uppercase text-muted"> Faktura</div>
                </div>
            </div>
        </a>
    </div>
    <!-- END Completed -->
</div>
<h2 class="content-heading">Promena statusa porudžbine</h2>
<div class="row row-deck gutters-tiny">
<!-- END Progress -->
<form method="POST" id="forma-akcije" >
    {{csrf_field()}}
    @if($porudzbina->status != 'nova')
        <button type="submit" class="text-warning btn btn-sm btn-secondary button-akcija" data-toggle="tooltip" title="Vrati u Nove" formaction="/admin/statusPorudzbine/{{$porudzbina->id}}/nova">
            <i class="fa fa-flag fa-3x"></i>
        </button>
    @endif
    @if($porudzbina->dostava and $porudzbina->status != 'poslata')
        <button type="submit" class="text-primary btn btn-sm btn-secondary button-akcija"  data-toggle="tooltip" title="Pošalji" formaction="/admin/statusPorudzbine/{{$porudzbina->id}}/poslata">
            <i class="fa fa-truck fa-3x"></i>
        </button>
    @endif

    @if($porudzbina->status != 'kompletirana')
        <button type="submit" class="text-success btn btn-sm btn-secondary button-akcija" data-toggle="tooltip" title="Kompletiraj - naplaćeno" formaction="/admin/statusPorudzbine/{{$porudzbina->id}}/kompletirana">
            <i class="fa fa-check fa-3x"></i>
        </button>
    @endif

    @if($porudzbina->status != 'stornirana')
        <button type="submit" class="text-danger btn btn-sm btn-secondary button-akcija"  data-toggle="tooltip" title="Storniraj" formaction="/admin/statusPorudzbine/{{$porudzbina->id}}/stornirana">
            <i class="fa fa-times fa-3x"></i>
        </button>
    @endif
</form>
</div>
<!-- Addresses -->
<h2 class="content-heading">Informacije</h2>
<div class="row row-deck gutters-tiny">
    <!-- Billing Address -->
    <div class="col-md-6">
        <div class="block block-rounded">
            <div class="block-header block-header-default">
                <h3 class="block-title">Podaci o kupcu&nbsp;@if($porudzbina->firma) (FIRMA) @endif</h3>
            </div>
            <div class="block-content">
                <div class="font-size-lg text-black mb-5">{{$porudzbina->kupac}}&nbsp;@if($porudzbina->firma) (PIB:&nbsp;{{$porudzbina->PIB}}) @endif</div>
                <address>
                    <i class="fa fa-home mr-5"></i> {{$porudzbina->adresa}},&nbsp;{{$porudzbina->grad}},&nbsp;{{$porudzbina->zip}}<br>
                    <i class="fa fa-phone mr-5"></i> {{$porudzbina->telefon}}<br>
                    <i class="fa fa-envelope-o mr-5"></i> <a href="mailto:{{$porudzbina->email}}"> {{$porudzbina->email}}</a>
                    <!-- ispisi ovde napomenu ako se podaci za dostavu ne ispisuju -->
                    @if(!$porudzbina->dostava and $porudzbina->napomena != '')
                        <br><br>
                        <i class="fa fa-edit mr-5"></i> Napomena kupca:<br>
                        {{$porudzbina->napomena}}
                    @endif
                    @if($porudzbina->id_user != null and $porudzbina->user->admin_napomena != '')
                        <br><br>
                        <i class="fa fa-edit mr-5"></i> Adminova napomena:<br>
                       {{$porudzbina->user->admin_napomena}}
                    @endif
                </address>
            </div>
        </div>
    </div>
    <!-- END Billing Address -->


    <!-- Shipping Address -->
    <div class="col-md-6">
        <div class="block block-rounded">
            <div class="block-header block-header-default">
                <h3 class="block-title">Podaci za isporuku</h3>
            </div>

            <div class="block-content">
                @if($porudzbina->dostava)
                <div class="font-size-lg text-black mb-5">{{$porudzbina->dostava_kupac}}</div>
                <address>
                    <i class="fa fa-home mr-5"></i>{{$porudzbina->dostava_adresa}},&nbsp;{{$porudzbina->dostava_grad}},&nbsp;{{$porudzbina->dostava_zip}}<br>
                    <i class="fa fa-phone mr-5"></i> {{$porudzbina->dostava_telefon}}<br>
                    @if($porudzbina->dostava_email != null)
                        <i class="fa fa-envelope-o mr-5"></i> <a href="mailto:{{$porudzbina->dostava_email}}"> {{$porudzbina->dostava_email}}</a>
                    @endif

                    @if($porudzbina->napomena != '')
                        <br><br>
                        <i class="fa fa-edit mr-5"></i> Napomena kupca:<br>
                        {{$porudzbina->napomena}}
                    @endif
                </address>
                @else
                    <div class="font-size-lg text-black mb-5">Kupac će lično preuzeti robu.</div>
                @endif
            </div>


        </div>
    </div>

    <!-- END Shipping Address -->
</div>
<br/>
<!-- END Addresses -->
<!-- END Customer's Basic Info -->

<!-- Products -->
<h2 class="content-heading">Porudžbina / Korpa ({{$brojProizvodaKorpa}})</h2>
<div class="block block-rounded">
    <div class="block-content">
        <div class="table-responsive">
            <table class="table table-borderless table-striped">
                <thead>
                    <tr>
                        <th style="width: 100px;">Šifra</th>
                        <th>Naziv</th>
                        <th class="text-center">Količina</th>
                        <th class="text-right" style="width: 10%;">Cena</th>
                        <th class="text-right" style="width: 10%;">Cena sa popustom</th>
                        <th class="text-right" style="width: 10%;">Ukupno</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($porudzbina->stavke as $stavka)
                    <tr>
                        <td>
                            <a class="font-w600">{{$stavka->sifra}}</a>
                        </td>
                        <td>
                            <a href="/admin/proizvod/{{$stavka->id_proizvod}}">{{$stavka->proizvod->naziv}}</a>&emsp;
                            @if($stavka->ima_opcije)
                                @foreach($stavka->opcije as $opcija)
                                    -&nbsp;Opcija&nbsp;{{$opcija->naziv}}&emsp;
                                @endforeach
                            @endif

                            @if($stavka->id_kupon != null)
                                -&nbsp;Kupon&nbsp;<a href="/admin/kupon/{{$stavka->id_kupon}}">{{$stavka->kupon->naziv}}</a>
                            @endif
                        </td>
                        <td class="text-center">{{$stavka->kolicina}}</td>
                        <td class="text-right">{{number_format($stavka->cena, 2, ',', '.')}}</td>
                        <td class="text-right">{{number_format($stavka->cena_popust, 2, ',', '.')}}</td>
                        <td class="text-right">{{number_format($stavka->ukupno_popust, 2, ',', '.')}}</td>
                    </tr>
                    @endforeach
                    @if($porudzbina->ima_vaucere)
                        @foreach($porudzbina->vauceri as $vaucer)
                            <tr>
                                <td colspan="4" class="text-right font-w600">Vaučer&nbsp;<a href="/admin/vaucer/{{$vaucer->id}}">{{$vaucer->naziv}}</a>&nbsp;:</td>
                                <td colspan="2" class="text-right">-{{number_format($vaucer->iznos, 2, ',', '.')}}</td>
                            </tr>
                        @endforeach
                    @endif
                    <tr>
                        <td colspan="4" class="text-right font-w600">Ukupno bez popusta:</td>
                        <td colspan="2" class="text-right">{{number_format($porudzbina->iznos, 2, ',', '.')}}</td>
                    </tr>
                    <tr>
                        <td colspan="4" class="text-right font-w600">Popust:</td>
                        <td colspan="2" class="text-right">{{number_format($porudzbina->iznos - $porudzbina->iznos_popust, 2, ',', '.')}}</td>
                    </tr>
                    @if($porudzbina->dostava)
                        <tr>
                            <td colspan="4" class="text-right font-w600">Dostava:</td>
                            <td colspan="2" class="text-right">{{number_format(240, 2, ',', '.')}}</td>
                        </tr>
                    @endif
                    <tr class="table-success">
                        <td colspan="4" class="text-right font-w600 text-uppercase">Ukupno:</td>
                        <td colspan="2" class="text-right font-w600">{{number_format($porudzbina->iznos_popust, 2, ',', '.')}}&nbsp;rsd&nbsp;@if($porudzbina->dostava) (+{{number_format(240, 2, ',', '.')}}&nbsp;dostava) @endif</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

<!-- END Products -->
<!-- Customer -->
@if($porudzbina->id_user != null)
<h2 class="content-heading">
    Kupac
</h2>
<div class="row row-deck">
    <!-- Customer's Basic Info -->
    <div class="col-lg-4">
        <a class="block block-rounded block-link-shadow text-center" href="/admin/korisnik/{{$porudzbina->user->id}}">
            <div class="block-content bg-gd-dusk">
                <div class="push">
                    <img class="img-avatar img-avatar-thumb" src="{{asset('assets/img/avatars/avatar15.jpg')}}" alt="">
                </div>
                <div class="pull-r-l pull-b py-10 bg-black-op-25">
                    <div class="font-w600 mb-5 text-white">
                        {{$porudzbina->user->ime_prezime}} <i class="fa fa-star text-warning"></i>
                    </div>
                    <div class="font-size-sm text-white-op"> @if($porudzbina->user->id_vrsta_korisnika == null) Običan korisnik @else {{$porudzbina->user->vrsta->naziv}} @endif</div>
                </div>
            </div>
            <div class="block-content">
                <div class="row items-push text-center">
                    <div class="col-4">
                        <div class="mb-5"><i class="si si-bag fa-2x"></i></div>
                        <div class="font-size-sm text-muted">{{$porudzbina->user->broj_porudzbina}} porudžbina</div>
                    </div>
                    <div class="col-4">
                        <div class="mb-5"><i class="si si-basket-loaded fa-2x"></i></div>
                        <div class="font-size-sm text-muted">{{$porudzbina->user->broj_porucenih_proizvoda}} proizvoda</div>
                    </div>
                    <div class="col-4">
                        <div class="mb-5"><i class="si si-wallet fa-2x"></i></div>
                        <div class="font-size-sm text-muted">{{number_format($porudzbina->user->promet, 2, ',', '.')}} rsd</div>
                    </div>
                </div>
            </div>
        </a>
    </div>

    <!-- Customer's Past Orders -->
    <div class="col-lg-8">
        <div class="block block-rounded">
            <div class="block-header block-header-default">
                <h3 class="block-title">Prethodne porudžbine</h3>
            </div>
            <div class="block-content">
                <table class="table table-borderless table-sm table-striped">
                    <tbody>
                        @foreach($kupacPorudzbine as $kupacPorudzbina)
                        <tr>
                            <td>
                                <a class="font-w600" href="/admin/porudzbina/{{$kupacPorudzbina->id}}">{{$kupacPorudzbina->id}}</a>
                            </td>
                            <td>
                                @if($kupacPorudzbina->status == 'nova')
                                    <span class="badge badge-warning">Nova</span>
                                @elseif($kupacPorudzbina->status == 'poslata')
                                    <span class="badge badge-primary">Poslata</span>
                                @elseif($kupacPorudzbina->status == 'stornirana')
                                    <span class="badge badge-danger">Stornirana</span>
                                @elseif($kupacPorudzbina->status == 'kompletirana')
                                    <span class="badge badge-success">Kompletirana</span>
                                @endif
                            </td>
                            <td class="d-none d-sm-table-cell">
                                {{date_format($kupacPorudzbina->created_at, "d.m.Y.")}}
                            </td>
                            <td class="d-none d-sm-table-cell">
                                <a href="/admin/korisnik/{{$porudzbina->user->id}}">{{$porudzbina->user->ime_prezime}}</a>
                            </td>
                            <td class="text-right">
                                <span class="text-black">{{number_format($kupacPorudzbina->iznos_popust, 2, ',', '.')}}&nbsp;rsd</span>
                            </td>
                        </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- END Customer's Past Orders -->
</div>
@endif
<!-- END Customer -->
@stop