@extends('admin.adminLayout')

@section('title')
Faktura - #{{$porudzbina->id}}
@stop

@section('breadcrumbs')
<a class="breadcrumb-item" href="/admin">Admin</a>
<a class="breadcrumb-item" href="/admin/fakture">Fakture</a>
<span class="breadcrumb-item active">Faktura - {{$porudzbina->id}}</span>
@stop

@section('heder-h1')
Faktura - #{{$porudzbina->id}} 
@stop


@section('main')
<!-- Invoice -->
<h2 class="content-heading d-print-none">
     <a href="/admin/porudzbina/{{$porudzbina->id}}" type="button" class="btn  btn-rounded btn-primary float-right">Pogledaj porudžbinu</a>
    Faktura - {{$porudzbina->id}}
</h2>
<div class="block">
    <div class="block-header block-header-default">
        <h3 class="block-title">#{{$porudzbina->id}}</h3>
        <div class="block-options">
            <!-- Print Page functionality is initialized in Codebase() -> uiHelperPrint() -->
            <button type="button" class="btn-block-option" onclick="Codebase.helpers('print-page');">
                <i class="si si-printer"></i> Štampaj fakturu
            </button>
            <button type="button" class="btn-block-option" data-toggle="block-option" data-action="fullscreen_toggle"></button>
            
        </div>
    </div>
    <div class="block-content">
        <!-- Invoice Info -->
        <div class="row my-20">
            <!-- Company Info -->
            <div class="col-6">
                <p class="h3">Naziv firme</p>
                <address>
                    Kneza Miloša 44<br>
                    11000, Beograd<br>
                    Srbija<br>
                    info@firma.com
                </address>
            </div>
            <!-- END Company Info -->

            <!-- Client Info -->
            <div class="col-6 text-right">
                <p class="h3">{{$porudzbina->kupac}}</p>
                <address>
                    {{$porudzbina->adresa}}<br>
                    {{$porudzbina->zip}},&nbsp;{{$porudzbina->grad}}<br>
                     Srbija<br>
                    {{$porudzbina->email}}
                </address>
            </div>
            <!-- END Client Info -->
        </div>
        <!-- END Invoice Info -->

        <!-- Table -->
        <div class="table-responsive push">
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th class="text-center" style="width: 5%;"></th>
                        <th style="width: 45%;">Proizvod</th>
                        <th class="text-center" style="width: 10%;">Količina</th>
                        <th class="text-right" style="width: 20%;">Jedinična cena</th>
                        <th class="text-right" style="width: 20%;">Ukupno</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i = 1; ?>
                    @foreach($porudzbina->stavke as $stavka)
                        <tr>
                            <td class="text-center">{{$i++}}</td>
                            <td>
                                <p class="font-w600 mb-5">{{$stavka->proizvod->naziv}}</p>
                                <div class="text-muted">{{$stavka->proizvod->kratak_opis}}</div>
                            </td>
                            <td class="text-center">
                                <span class="badge badge-pill badge-primary">{{$stavka->kolicina}}</span>
                            </td>
                            <td class="text-right">{{number_format($stavka->ukupno_popust/$stavka->kolicina, 2, ',', '.')}}</td>
                            <td class="text-right">{{number_format($stavka->ukupno_popust, 2, ',', '.')}}</td>
                        </tr>
                    @endforeach
                    @if($porudzbina->ima_vaucere)
                        @foreach($porudzbina->vauceri as $vaucer)
                            <tr>
                                <td colspan="4" class="text-right font-w600">Vaučer&nbsp;{{$vaucer->naziv}}:</td>
                                <td colspan="2" class="text-right">-{{number_format($vaucer->iznos, 2, ',', '.')}}</td>
                            </tr>
                        @endforeach
                    @endif
                    <tr>
                        <td colspan="4" class="font-w600 text-right">Ukupno bez PDV</td>
                        <td class="text-right">{{number_format($porudzbina->iznos_popust/1.2, 2, ',', '.')}}</td>
                    </tr>
                    <tr>
                        <td colspan="4" class="font-w600 text-right">PDV 20%</td>
                        <td class="text-right">{{number_format($porudzbina->iznos_popust - $porudzbina->iznos_popust/1.2, 2, ',', '.')}}</td>
                    </tr>
                    <tr class="table-warning">
                        <td colspan="4" class="font-w700 text-uppercase text-right">Ukupno sa PDV</td>
                        <td class="font-w700 text-right">{{number_format($porudzbina->iznos_popust, 2, ',', '.')}}&nbsp;rsd</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <!-- END Table -->

        <!-- Footer -->
        <p class="text-muted text-center">Ova faktura je važeća bez potpisa i pečata.</p>
        <!-- END Footer -->
    </div>
</div>
<!-- END Invoice -->

@stop