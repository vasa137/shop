@extends('admin.adminLayout')

@section('title')
Vaučer - @if($izmena) {{$vaucer->naziv}} @else Novi vaučer @endif
@stop

@section('breadcrumbs')
<a class="breadcrumb-item" href="/admin">Admin</a>
<a class="breadcrumb-item" href="/admin/vauceri">Vaučeri</a>
<span class="breadcrumb-item active">@if($izmena) {{$vaucer->naziv}} @else Novi vaučer @endif</span>
@stop

@section('heder-h1')
@if($izmena) {{$vaucer->naziv}} @else Novi vaučer @endif
@stop


@section('main')
<div class="row gutters-tiny">
@if($izmena)

    @if(!$vaucer->sakriven and $vaucer->aktivan)
            <!-- Delete Product -->
            <div class="col-xl-4">
                <form id="forma-deaktiviraj" action="/admin/iskoristiVaucer/{{$vaucer->id}}" method="POST">
                    {{csrf_field()}}
                    <a class="block block-rounded block-link-shadow" href="javascript: document.getElementById('forma-deaktiviraj').submit()">
                        <div class="block-content block-content-full block-sticky-options">
                            <div class="block-options">
                                <div class="block-options-item">
                                    <i class="fa fa-tag fa-2x text-success-light"></i>
                                </div>
                            </div>
                            <div class="py-20 text-center">
                                <div class="font-size-h2 font-w700 mb-0 text-success">
                                    <i class="fa fa-money"></i>
                                </div>
                                <div class="font-size-sm font-w600 text-uppercase text-muted">Iskoristi vaučer</div>
                            </div>
                        </div>
                    </a>
                </form>
            </div>
    @endif



        @if(!$vaucer->sakriven)
        <!-- Delete Product -->
            <div class="col-xl-4">
                <form id="forma-obrisi" action="/admin/obrisiVaucer/{{$vaucer->id}}" method="POST">
                    {{csrf_field()}}
                    <a class="block block-rounded block-link-shadow" href="javascript: document.getElementById('forma-obrisi').submit()">
                        <div class="block-content block-content-full block-sticky-options">
                            <div class="block-options">
                                <div class="block-options-item">
                                    <i class="fa fa-archive fa-2x text-danger-light"></i>
                                </div>
                            </div>
                            <div class="py-20 text-center">
                                <div class="font-size-h2 font-w700 mb-0 text-danger">
                                    <i class="fa fa-times"></i>
                                </div>
                                <div class="font-size-sm font-w600 text-uppercase text-muted">Obriši vaučer</div>
                            </div>
                        </div>
                    </a>
                </form>
            </div>
        @else
            <div class="col-md-6 col-xl-4">
                <form id="forma-restauriraj" action="/admin/restaurirajVaucer/{{$vaucer->id}}" method="POST">
                    {{csrf_field()}}
                    <a class="block block-rounded block-link-shadow" href="javascript:document.getElementById('forma-restauriraj').submit()">
                        <div class="block-content block-content-full block-sticky-options">
                            <div class="block-options">
                                <div class="block-options-item">
                                    <i class="fa fa-shopping-cart fa-2x text-warning-light"></i>
                                </div>
                            </div>
                            <div class="py-20 text-center">
                                <div class="font-size-h2 font-w700 mb-0 text-warning">
                                    <i class="fa fa-undo"></i>
                                </div>
                                <div class="font-size-sm font-w600 text-uppercase text-muted">Restauriraj vaučer</div>
                            </div>
                        </div>
                    </a>
                </form>
            </div>
        @endif
    @endif

    <!-- END Delete Product -->
    <div class="col-md-6 col-xl-4">
        <a class="block block-rounded block-link-shadow" href="javascript: document.getElementById('forma-vauceri-submit-button').click()">
            <div class="block-content block-content-full block-sticky-options">
                <div class="block-options">
                    <div class="block-options-item">
                        <i class="si si-settings fa-2x text-success"></i>
                    </div>
                </div>
                <div class="py-20 text-center">
                    <div class="font-size-h2 font-w700 mb-0 text-success">
                        <i class="fa fa-check"></i>
                    </div>
                    <div class="font-size-sm font-w600 text-uppercase ">Sačuvaj</div>
                </div>
            </div>
        </a>
    </div>

</div>
<!-- END Overview -->
@if($errors->has('greska'))
    <p class="offset-4 col-7" style="color:red; font-weight:bold;">{{$errors->first('greska')}}</p>
@endif
<form id="forma-vaucer" @if(!$izmena) action="/admin/sacuvajVaucer/-1" @else action="/admin/sacuvajVaucer/{{$vaucer->id}}" @endif method="POST" >
    {{csrf_field()}}
<div class="row gutters-tiny">
    <!-- Basic Info -->
    <div class="col-md-7">
            <div class="block block-rounded block-themed">
                <div class="block-header bg-gd-primary">
                    <h3 class="block-title">VAUČER</h3>
                </div>
                <div class="block-content block-content-full">
                	<div class="form-group row">
                        <label class="col-12" >Naziv</label>
                        <div class="col-12">
                            <input type="text" class="form-control" name="naziv" @if($izmena) value="{{$vaucer->naziv}}" @endif >
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-12">Kod vaučera (kod za ostvarivanje popusta)</label>
                        <div class="col-12">
                            <input type="text" class="form-control" name="kod" @if($izmena) value="{{$vaucer->kod}}" @endif >
                        </div>
                    </div>
                    
                </div>
            </div>
    </div>
    <!-- END Basic Info -->
    
    <!-- More Options -->
    <div class="col-md-5">
        <!-- Status -->
            <div class="block block-rounded block-themed">
                <div class="block-header bg-gd-primary">
                    <h3 class="block-title">VREDNOST VAUČERA</h3>
                    
                </div>

                <div class="block-content block-content-full">						
                    <div class="form-group row">
                        <label class="col-12">Iznos:</label>
                        
                        <div class="col-12">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="fa fa-fw fa-money"></i>
                                    </span>
                                </div>
                                <input type="number" min="1" class="form-control" id="ecom-product-price" name="iznos" @if($izmena) value="{{$vaucer->iznos}}" readonly @endif required>
                                <div class="input-group-append">
                                    <span class="input-group-text">rsd</span>
                                </div>
                            </div>
                        </div>
                        
                        
                    </div>

                    @if($izmena)
                    <div class="form-group row">
                        <br>
                        <div class="col-sm-12">
                            <br/><br/>
                            <label> Status:&emsp; <span @if($vaucer->aktivan) class="text-success" @else class="text-danger" @endif> @if($vaucer->aktivan) Nije iskorišćen @else Iskorišćen @endif</span></label>
                        </div>
                    </div>
                    @endif

                </div>
            </div>

        <!-- END Status -->
    </div>
    <!-- END More Options -->

</div>
<!-- END Update Product -->
<input type="submit" style="display:none" id="forma-vauceri-submit-button"/>
</form>
@stop