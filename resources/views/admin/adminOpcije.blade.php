@extends('admin.adminLayout')

@section('title')
    Opcije
@stop

@section('breadcrumbs')
    <a class="breadcrumb-item" href="/admin">Admin</a>
    <span class="breadcrumb-item active">Opcije</span>
@stop

@section('heder-h1')
Opcije
@stop


@section('heder-h2')
Trenutno <a class="text-primary-light link-effect">{{$brojAktivnihOpcija}} aktivnih opcija</a>.
@stop

@section('scriptsBottom')
    <!-- Page JS Code -->
    <script src="{{asset('/js/tabelaOpcije.js')}}"></script>
@endsection

@section('main')
    <div class="row gutters-tiny">
        <!-- All Products -->
        <div class="col-md-6 col-xl-3">
            <a class="block block-rounded block-link-shadow">
                <div class="block-content block-content-full block-sticky-options">
                    <div class="block-options">
                        <div class="block-options-item">
                            <i class="fa fa-circle-o fa-2x text-info-light"></i>
                        </div>
                    </div>
                    <div class="py-20 text-center">
                        <div class="font-size-h2 font-w700 mb-0 text-info" data-toggle="countTo" data-to="{{count($opcije)}}">0</div>
                        <div class="font-size-sm font-w600 text-uppercase text-muted">Ukupno opcija</div>
                    </div>
                </div>
            </a>
        </div>
        <!-- END All Products -->

        <!-- Top Sellers -->
        <div class="col-md-6 col-xl-3">
            <a class="block block-rounded block-link-shadow" >
                <div class="block-content block-content-full block-sticky-options">
                    <div class="block-options">
                        <div class="block-options-item">
                            <i class="fa fa-star fa-2x text-warning-light"></i>
                        </div>
                    </div>
                    <div class="py-20 text-center">
                        <div class="font-size-h2 font-w700 mb-0 text-warning" data-toggle="countTo" data-to="{{count($grupeOpcija)}}">0</div>
                        <div class="font-size-sm font-w600 text-uppercase text-muted">Ukupno grupa opcija</div>
                    </div>
                </div>
            </a>
        </div>
        <!-- END Top Sellers -->

        <!-- Add Product -->
        <div class="col-md-6 col-xl-3">
            <a class="block block-rounded block-link-shadow" href="/admin/opcija/-1">
                <div class="block-content block-content-full block-sticky-options">
                    <div class="block-options">
                        <div class="block-options-item">
                            <i class="fa fa-archive fa-2x text-success-light"></i>
                        </div>
                    </div>
                    <div class="py-20 text-center">
                        <div class="font-size-h2 font-w700 mb-0 text-success">
                            <i class="fa fa-plus"></i>
                        </div>
                        <div class="font-size-sm font-w600 text-uppercase text-muted">Dodaj novu opciju</div>
                    </div>
                </div>
            </a>
        </div>
        <!-- END Add Product -->
    </div>
    <!-- END Overview -->
    @foreach($grupeOpcija as $grupaOpcija)
    <!-- Dynamic Table Full Pagination -->
    <div class="block">

        <div class="block-header block-header-default">
            <h3 id="grupa-opcija-{{$grupaOpcija->id}}-title" class="block-title">{{$grupaOpcija->naziv}}</h3>
        </div>

        <div class="block-content block-content-full">


                <!-- DataTables init on table by adding .js-dataTable-full-pagination class, functionality initialized in js/pages/be_tables_datatables.js -->
                    <table id="tabela-grupaOpcija-{{$grupaOpcija->id}}" class="table table-bordered table-striped table-vcenter js-dataTable-full">
                        <thead>
                        <tr>
                            <th style="width:40%;">Naziv</th>
                            <th class="d-none d-sm-table-cell text-center" style="width:20%;">Različitih proizvoda</th>
                            <th class="d-none d-sm-table-cell text-center" style="width:20%;">Status</th>
                            <th class="text-center" style="width:20%;">Akcija</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($grupaOpcija->opcije as $opcija)
                            <tr>
                                <td class="font-w600">{{$opcija->naziv}}</td>
                                <td class="d-none d-sm-table-cell text-center">{{$opcija->broj_proizvoda}}</td>
                                <td class="d-none d-sm-table-cell text-center"><span @if(!$opcija->sakriven) class="text-success" @else class="text-danger" @endif> @if(!$opcija->sakriven) Aktivna @else Obrisana @endif</span></td>

                                <td class="text-center">
                                    <a href="/admin/opcija/{{$opcija->id}}" class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Izmeni opciju">
                                        <i class="fa fa-edit"></i>
                                    </a>

                                    @if(!$opcija->sakriven)
                                        <form method="POST" action="/admin/obrisiOpciju/{{$opcija->id}}" style="display:inline">
                                            {{csrf_field()}}
                                            <button type="submit" class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Obriši opciju">
                                                <i class="fa fa-times"></i>
                                            </button>
                                        </form>
                                    @else
                                        <form method="POST" action="/admin/restaurirajOpciju/{{$opcija->id}}" style="display:inline">
                                            {{csrf_field()}}
                                            <button type="submit" class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Restauriraj opciju">
                                                <i class="fa fa-undo"></i>
                                            </button>
                                        </form>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>




        </div>

    </div>
    @endforeach
@stop