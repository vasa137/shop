@extends('admin.adminLayout')

@section('title')
@if($izmena) {{$proizvod->naziv}} @else Novi proizvod @endif
@stop

@section('breadcrumbs')
<a class="breadcrumb-item" href="/admin">Admin</a>
<a class="breadcrumb-item" href="/admin/proizvodi">Proizvodi</a>
<span class="breadcrumb-item active">@if($izmena) {{$proizvod->naziv}} @else Novi proizvod @endif</span>
@stop

@section('heder-h1')
@if($izmena) {{$proizvod->naziv}} @else Novi proizvod @endif
@stop


@section('heder-h2')

@stop

@section('scriptsTop')
    <link rel="stylesheet" href="{{asset('css/bootstrap-treeview.css')}}">
    <link rel="stylesheet" href="{{asset('css/select2.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/js/plugins/dropzonejs/dropzone.css')}}">
    <script src="{{asset('js/adminProizvod.js')}}"></script>
@stop

@section('scriptsBottom')
    <script src="{{asset('js/bootstrap-treeview.js')}}"></script>
    <script src="{{asset('js/kategorija.js')}}"></script>
    <script src="{{asset('js/select2.min.js')}}"></script>
    <script src="{{asset('assets/js/plugins/dropzonejs/dropzone.js')}}"></script>
    <script src="{{asset('js/dropzoneSlike.js')}}"></script>

    <!-- Da se ne bi pojavljivali &quot karateri radimo laravel ispis sa !! -->
    <script>
        inicijalizujSpecifikacije('{!! addslashes(json_encode($specifikacije))!!}');
        @if(isset($opcijePoGrupama))
            inicijalizujOpcije('{!! addslashes(json_encode($grupeOpcija)) !!}','{!! addslashes(json_encode($opcijePoGrupama)) !!}');
        @endif
        $('.js-example-basic-multiple').select2();

        @if(!$izmena)
            inicijalizujKategorije('{!! addslashes(json_encode($stabloKategorija)) !!}');
        @else
            inicijalizujKategorije('{!! addSlashes(json_encode($stabloKategorija)) !!}', '{!! addslashes(json_encode($proizvod->kategorije))!!}');
            postaviPolja('{!! addSlashes(json_encode($proizvod))!!}');
        @endif




    </script>


@stop

@section('main')
<div class="row gutters-tiny">
@if($izmena)

    <!-- In Orders -->
    <div class="col-md-6 col-xl-4">
        <a class="block block-rounded block-link-shadow">
            <div class="block-content block-content-full block-sticky-options">
                <div class="block-options">
                    <div class="block-options-item">
                        <i class="fa fa-shopping-basket fa-2x text-info"></i>
                    </div>
                </div>
                <div class="py-20 text-center">
                    <div class="font-size-h2 font-w700 mb-0 text-info" data-toggle="countTo" data-to="{{$proizvod->broj_porucivanja}}">0</div>
                    <div class="font-size-sm font-w600 text-uppercase text-muted">puta naručen</div>
                </div>
            </div>
        </a>
    </div>
    <!-- END In Orders -->



    @if(!$proizvod->sakriven)
    <!-- Delete Product -->
    <div class="col-xl-4">
        <form id="forma-obrisi" action="/admin/obrisiProizvod/{{$proizvod->id}}" method="POST">
            {{csrf_field()}}
            <a class="block block-rounded block-link-shadow" href="javascript: document.getElementById('forma-obrisi').submit()">
                <div class="block-content block-content-full block-sticky-options">
                    <div class="block-options">
                        <div class="block-options-item">
                            <i class="fa fa-archive fa-2x text-danger-light"></i>
                        </div>
                    </div>
                    <div class="py-20 text-center">
                        <div class="font-size-h2 font-w700 mb-0 text-danger">
                            <i class="fa fa-times"></i>
                        </div>
                        <div class="font-size-sm font-w600 text-uppercase text-muted">Obriši Proizvod</div>
                    </div>
                </div>
            </a>
        </form>
    </div>
    @else
    <div class="col-md-6 col-xl-4">
        <form id="forma-restauriraj" action="/admin/restaurirajProizvod/{{$proizvod->id}}" method="POST">
        {{csrf_field()}}
        <a class="block block-rounded block-link-shadow" href="javascript:document.getElementById('forma-restauriraj').submit()">
            <div class="block-content block-content-full block-sticky-options">
                <div class="block-options">
                    <div class="block-options-item">
                        <i class="fa fa-shopping-cart fa-2x text-warning-light"></i>
                    </div>
                </div>
                <div class="py-20 text-center">
                    <div class="font-size-h2 font-w700 mb-0 text-warning">
                        <i class="fa fa-undo"></i>
                    </div>
                    <div class="font-size-sm font-w600 text-uppercase text-muted">Restauriraj Proizvod</div>
                </div>
            </div>
        </a>
        </form>
    </div>
    @endif

    <!-- END Delete Product -->


@endif

<!-- Stock -->
<div class="col-md-6 col-xl-4">
    <a class="block block-rounded block-link-shadow" href="javascript:document.getElementById('forma-proizvod-submit-button').click()">
        <div class="block-content block-content-full block-sticky-options">
            <div class="block-options">
                <div class="block-options-item">
                    <i class="si si-settings fa-2x text-success"></i>
                </div>
            </div>
            <div class="py-20 text-center">
                <div class="font-size-h2 font-w700 mb-0 text-success">
                    <i class="fa fa-check"></i>
                </div>
                <div class="font-size-sm font-w600 text-uppercase ">Sačuvaj</div>
            </div>
        </div>
    </a>
</div>
</div>
<!-- END Stock -->
<!-- END Overview -->
<form id="proizvodForma" @if($izmena) action="/admin/sacuvajProizvod/{{$proizvod->id}}" @else action="/admin/sacuvajProizvod/-1" @endif method="POST" onsubmit="return proizvodFormaSubmit()">
{{csrf_field()}}
<!-- Update Product -->
<h2 class="content-heading">Izmenite proizvod</h2>
<div class="row gutters-tiny">
    <!-- Basic Info -->
    <div id="levaStranaKontejner" class="col-md-7">
            <div class="block block-rounded block-themed">
                <div class="block-header bg-gd-primary">
                    <h3 class="block-title">Osnovne informacije</h3>
                </div>
                <div class="block-content block-content-full">
                	<div class="form-group row">
                        <label class="col-12">Naziv</label>
                        <div class="col-12">
                            <input type="text" maxlength="254" class="form-control" name="naziv"  @if($izmena) value="{{$proizvod->naziv}}" @endif required/>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-12">Šifra proizvoda</label>
                        <div class="col-12">
                            <input type="text" maxlength="254" class="form-control" name="sifra" @if($izmena) value="{{$proizvod->sifra}}" @else value="0" @endif required/>
                        </div>
                    </div>
                    
                    <div class="form-group row">
                        <label class="col-12" for="example-select2">Brend</label>
                        <div class="col-12">
                            <!-- Select2 (.js-select2 class is initialized in Codebase() -> uiHelperSelect2()) -->
                            <!-- For more info and examples you can check out https://github.com/select2/select2 -->

                            <select id="brendSelect" class="js-select2 form-control" name="brend" style="width: 100%;" data-placeholder="Choose one..">
                                <option></option><!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                @foreach($brendovi as $brend)
                                    <option value="{{$brend->id}}" @if($izmena and $proizvod->id_brend != null and $proizvod->brend->id == $brend->id) selected @endif>{{$brend->naziv}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>


                    <div class="form-group row">
                        <label class="col-12" for="example-select22">Dobavljač</label>
                        <div class="col-12">
                            <!-- Select2 (.js-select2 class is initialized in Codebase() -> uiHelperSelect2()) -->
                            <!-- For more info and examples you can check out https://github.com/select2/select2 -->
                            <select id="dobavljacSelect" class="js-select2 form-control" name="dobavljac" style="width: 100%;" data-placeholder="Choose one..">
                                <option></option><!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                @foreach($dobavljaci as $dobavljac)
                                    <option value="{{$dobavljac->id}}" @if($izmena and $proizvod->id_dobavljac != null and $proizvod->dobavljac->id == $dobavljac->id) selected @endif>{{$dobavljac->naziv}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">

                        <label class="col-12">Opis</label>
                        <div class="col-12">
                            <!-- CKEditor (js-ckeditor id is initialized in Codebase() -> uiHelperCkeditor()) -->
                            <!-- For more info and examples you can check out http://ckeditor.com -->
                            <textarea maxlength="9999" class="form-control" id="opisTA" name="opis" rows="8">@if($izmena){{$proizvod->opis}}@endif</textarea>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-12">Kratak opis</label>
                        <div class="col-12">
                            <!-- CKEditor (js-ckeditor id is initialized in Codebase() -> uiHelperCkeditor()) -->
                            <!-- For more info and examples you can check out http://ckeditor.com -->
                            <textarea maxlength="999" class="form-control" id="kratakOpisTA" name="kratak_opis" rows="4">@if($izmena){{$proizvod->kratak_opis}}@endif</textarea>
                        </div>
                    </div>
                </div>
            </div>



        <div class="block block-rounded block-themed">
            <div class="block-header bg-gd-primary">
                <h3 class="block-title">Kategorije</h3>
            </div>
            <div class="block-content block-content-full row">
                <div class="col-sm-12">
                    <div id="treeview-selectable"></div>
                </div>
            </div>
        </div>



        <div class="block block-rounded block-themed">
            <div class="block-header bg-gd-primary">
                <h3 class="block-title">Specifikacije</h3>
            </div>
            <div class="block-content block-content-full row">
            <div class="col-12">
                <!-- Select2 (.js-select2 class is initialized in Codebase() -> uiHelperSelect2()) -->
                <!-- For more info and examples you can check out https://github.com/select2/select2 -->
                <select id="specifikacije" size="4" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
                    @foreach($specifikacije as $spec)
                        @if(!$izmena or !$proizvod->ima_specifikacije or !in_array($spec->id, $proizvod->specifikacije))
                            <option id="specifikacija-{{$spec->id}}" value="{{$spec->id}}" @if($spec == $specifikacije[0]) selected @endif>{{$spec->naziv}}</option>
                        @endif
                    @endforeach
                </select>
                <br/>
            </div>

            <div class="col-4 offset-4">

                <button onclick="dodajSpecifikaciju()" type="button" class="btn btn-success mr-5 mb-5">
                    <i class="fa fa-plus mr-5"></i>Dodaj novu specifikaciju
                </button>
                <br/><br/>
            </div>

            <div id="blokSpecifikacija" class="col-12">

            </div>
            </div>
        </div>

        <div class="block block-rounded block-themed">
            <div class="block-header bg-gd-primary">
                <h3 class="block-title">Grupe Opcija</h3>
            </div>
            <div class="block-content block-content-full row">
            <div class="col-12">
                <!-- Select2 (.js-select2 class is initialized in Codebase() -> uiHelperSelect2()) -->
                <!-- For more info and examples you can check out https://github.com/select2/select2 -->
                <select id="grupeOpcija" size="4" class="js-select2 form-control" name="grupeOpcija" style="width: 100%;" data-placeholder="Choose one..">
                    @foreach($grupeOpcija as $grupa)
                        @if(isset($opcijePoGrupama) and isset($opcijePoGrupama[$grupa->id]) and (!$izmena  or (!$proizvod->ima_opcije or !in_array($grupa->id, $proizvod->grupe_opcija))))
                             <option value="{{$grupa->id}}" @if($grupa == $grupeOpcija[0]) selected @endif>{{$grupa->naziv}}</option>
                        @endif
                    @endforeach
                </select>
                <br/>
            </div>

            <div class="col-4 offset-4">

                <button onclick="dodajGrupuOpcija()" type="button" class="btn btn-success mr-5 mb-5">
                    <i class="fa fa-plus mr-5"></i>Dodaj novu grupu opcija
                </button>
                <br/><br/>
            </div>
            <div id="blokGrupaOpcija" class="col-12">

            </div>

            </div>
        </div>

    </div>

    <!-- END Basic Info -->
    
    <!-- More Options -->

    <div id="desnoKontejner" class="col-md-5">
        <!-- Status -->

        <div class="block block-rounded block-themed">
            <div class="block-header bg-gd-primary">
                <h3 class="block-title">Status</h3>

            </div>

            <div class="block-content block-content-full">

                <div class="form-group row">
                    <label class="col-6" >Nabavna Cena</label>
                    <label class="col-6" >Cena</label>

                    <div class="col-sm-6">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="fa fa-fw fa-money"></i>
                                </span>
                            </div>
                            <input type="number" class="form-control" name="nabavna_cena" @if($izmena) value="{{$proizvod->nabavna_cena}}" @else value="0" @endif required>
                            <div class="input-group-append">
                                <span class="input-group-text">rsd</span>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="fa fa-fw fa-money"></i>
                                </span>
                            </div>
                            <input type="number" class="form-control" name="cena" @if($izmena) value="{{$proizvod->cena}}" @else value="0" @endif required>
                            <div class="input-group-append">
                                <span class="input-group-text">rsd</span>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="form-group row">
                    <label class="col-12" for="ecom-product-price">Cena sa popustom</label>
                    <div class="col-sm-6">
                        <div class="input-group custom-control custom-checkbox">
                            <input class="custom-control-input" id="na_popustuCB" type="checkbox" name="na_popustu" value="1" @if($izmena and $proizvod->na_popustu == 1) checked @endif/>
                            <label class="custom-control-label" for="na_popustuCB">Proizvod je na sniženju?</label>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="fa fa-fw fa-percent"></i>
                                </span>
                            </div>
                            <input type="number" class="form-control"  name="cena_popust" @if($izmena) value="{{$proizvod->cena_popust}}" @else value="0" @endif required>
                            <div class="input-group-append">
                                <span class="input-group-text">rsd</span>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="form-group row">
                    <label class="col-12" >Lager</label>

                    <div class="col-12">
                        <label class="css-control css-control-primary css-radio">
                            <input type="radio" class="css-control-input" name="lager" value="na_stanju" @if(!$izmena or ($izmena and $proizvod->lager == 'na_stanju')) checked @endif >
                            <span class="css-control-indicator"></span> Dostupno
                        </label>
                        <label class="css-control css-control-secondary css-radio">
                            <input type="radio" class="css-control-input" name="lager" value="van_stanja" @if($izmena and $proizvod->lager == 'van_stanja') checked @endif >
                            <span class="css-control-indicator"></span> Nedostupno
                        </label>
                        <label class="css-control css-control-secondary css-radio">
                            <input type="radio" class="css-control-input" name="lager" value="ne_prikazuj" @if($izmena and $proizvod->lager == 'ne_prikazuj') checked @endif>
                            <span class="css-control-indicator"></span> Ne prikazuj
                        </label>
                        <label class="css-control css-control-secondary css-radio">
                            <input type="radio" class="css-control-input" name="lager" value="prati_broj_komada" @if($izmena and $proizvod->lager == 'prati_broj_komada') checked @endif>
                            <span class="css-control-indicator"></span> Prati na osnovu količine
                        </label>
                    </div>
                    <label class="col-12" >Količina na stanju</label>
                    <div class="col-sm-12">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="fa fa-fw fa-archive"></i>
                                </span>
                            </div>
                            <input type="number" class="form-control" name="br_komada" @if($izmena) value="{{$proizvod->br_komada}}" @else value="0" @endif required>
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-12">Status</label>
                    <div class="col-12">
                        @foreach($opisiProizvoda as $opis)
                            <div class="custom-control custom-checkbox custom-control-inline mb-5">
                                <input class="custom-control-input" type="checkbox" name="opisiProizvoda[]" id="opisProizvoda-{{$opis->id}}" value="{{$opis->id}}" @if($izmena and $proizvod->opisi[$opis->id]) checked @endif >
                                <label class="custom-control-label" for="opisProizvoda-{{$opis->id}}">{{$opis->opis}}</label>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        <!-- END Status -->

        <!-- Meta Data -->
        <div class="block block-rounded block-themed">
            <div class="block-header bg-gd-primary">
                <h3 class="block-title">Meta Data</h3>

            </div>
            <div class="block-content">
                <!-- Bootstrap Maxlength (.js-maxlength class is initialized in Codebase() -> uiHelperMaxlength()) -->
                <!-- For more info and examples you can check out https://github.com/mimo84/bootstrap-maxlength -->
                <div class="form-group row">
                    <label class="col-12">Title</label>
                    <div class="col-12">
                        <input type="text" class="js-maxlength form-control" name="meta_title" maxlength="254" data-always-show="true" data-placement="top" @if($izmena) value="{{$proizvod->meta_title}}" @endif>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-12" >Tags</label>
                    <div class="col-12">
                        <!-- jQuery Tags Input (.js-tags-input class is initialized in Codebase() -> uiHelperTagsInput()) -->
                        <!-- For more info and examples you can check out https://github.com/xoxco/jQuery-Tags-Input -->
                        <input type="text" class="js-tags-input form-control" data-height="34px"  name="meta_tags" maxlength="254" placeholder="ps4, rpg, action" @if($izmena) value="{{$proizvod->meta_tags}}" @endif>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-12">Description</label>
                    <div class="col-12">
                        <textarea class="js-maxlength form-control" name="meta_desc" maxlength="999" data-always-show="true" data-placement="top" rows="3">@if($izmena){{$proizvod->meta_desc}} @endif</textarea>
                    </div>
                </div>
            </div>
        </div>
        <!-- END Meta Data -->

        <div class="block block-rounded block-themed">
            <div class="block-header bg-gd-primary">
                <h3 class="block-title">Tagovi</h3>

            </div>
            <div class="block-content">
                <div class="block block-rounded block-themed">
                    <select id="tagovi" class="js-example-basic-multiple col-12" name="tagovi[]" multiple="multiple">
                        @foreach($tagovi as $tag)
                            <option value="{{$tag->id}}">{{$tag->naziv}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <br/>
        </div>
        <div class="block block-rounded block-themed">
            <div class="block-header bg-gd-primary">
                <h3 class="block-title">Glavna slika</h3>
            </div>
            <div class="block-content block-content-full">

                @if($izmena and File::exists(public_path('/images/proizvodi/temp/glavna/glavna.jpg')))
                <!-- Existing Images -->
                <div id="slike-opcija-0-glavna.jpg" class="row gutters-tiny items-push">
                    <div class="col-sm-12 col-xl-12">
                        <div class="options-container">
                            <img class="img-fluid options-item" src="{{asset('images/proizvodi/temp/glavna/glavna.jpg')}}" alt="">
                            <div class="options-overlay bg-black-op-75">
                                <div class="options-overlay-content">
                                    <a class="btn btn-sm btn-rounded btn-alt-danger min-width-75" href="javascript: obrisiSliku('glavna.jpg', 0);">
                                        <i class="fa fa-times"></i> Obriši
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END Existing Images -->
                @endif
                <div class="dropzone" id="dropzone-0" name="mainFileUploader">
                    <input type="hidden" id="opcija-dropzone-0" name="opcija-dropzone-0" value="0"/>
                    <div class="fallback">
                        <input name="file" type="file" />
                    </div>
                </div>

            </div>
        </div>
        <!-- Images -->
        <div class="block block-rounded block-themed">
            <div class="block-header bg-gd-primary">
                <h3 class="block-title">Slike</h3>
            </div>
            <div class="block-content block-content-full">
                <!-- Existing Images -->
                <div id="slike-opcija--1" class="row gutters-tiny items-push">
                    @if($izmena)
                        @foreach($proizvod->sveSlike as $slika)
                            <div id="slike-opcija--1-{{$slika}}" class="col-sm-6 col-xl-4">
                                <div class="options-container">
                                    <img class="img-fluid options-item" src="{{asset('images/proizvodi/temp/sveSlike/' . $slika)}}" alt="">
                                    <div class="options-overlay bg-black-op-75">
                                        <div class="options-overlay-content">
                                            <a class="btn btn-sm btn-rounded btn-alt-danger min-width-75" href="javascript:obrisiSliku('{{$slika}}', -1);">
                                                <i class="fa fa-times"></i> Obriši
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>

                <div class="dropzone" id="dropzone--1" name="mainFileUploader">
                    <input type="hidden" id="opcija-dropzone--1" name="opcija-dropzone--1" value="-1"/>
                    <div class="fallback">
                        <input name="file" type="file" multiple />
                    </div>
                </div>

            </div>
        </div>
        <!-- END Images -->
    </div>
    <!-- END More Options -->
</div>
<br/><br/>
<!-- END Update Product -->
    <input type="submit" style="display:none" id="forma-proizvod-submit-button"/>
</form>
@stop