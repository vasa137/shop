<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//---------KORISNICI RUTE POCETAK----------------
Route::get('/', function () {
    return view('welcome');
});


//---------KORISNICI RUTE KRAJ----------------


//---------ADMIN RUTE POCETAK----------------
Route::middleware(['admin'])->group(function () {
    Route::get('/admin', 'adminController@naslovna');

//--------------PROIZVODI---------------------
    Route::get('/admin/proizvodi', 'adminProizvodiController@proizvodi');

    Route::get('/admin/proizvod/{id}', 'adminProizvodiController@proizvod');


    Route::post('/admin/sacuvajProizvod/{id}', 'adminProizvodiController@sacuvaj_proizvod'); //za dodavanje novog ili izmene

    Route::post('/admin/obrisiProizvod/{id}', 'adminProizvodiController@obrisi_proizvod');

    Route::post('/admin/restaurirajProizvod/{id}', 'adminProizvodiController@restauriraj_proizvod');

    Route::post('/admin/uploadSlike', 'adminProizvodiController@upload_slike');

    Route::post('/admin/obrisiUploadSlike', 'adminProizvodiController@obrisi_upload_slike');

//--------------DOBAVLJAČI---------------------
    Route::get('/admin/dobavljaci', 'adminDobavljaciController@dobavljaci');

    Route::get('/admin/dobavljac/{id}', 'adminDobavljaciController@dobavljac');

    Route::post('/admin/sacuvajDobavljaca/{id}', 'adminDobavljaciController@sacuvaj_dobavljaca');

    Route::post('/admin/obrisiDobavljaca/{id}', 'adminDobavljaciController@obrisi_dobavljaca');

    Route::post('/admin/restaurirajDobavljaca/{id}', 'adminDobavljaciController@restauriraj_dobavljaca');
//--------------BRENDOVI---------------------
    Route::get('/admin/brendovi', 'adminBrendoviController@brendovi');

    Route::get('/admin/brend/{id}', 'adminBrendoviController@brend');

    Route::post('/admin/sacuvajBrend/{id}', 'adminBrendoviController@sacuvaj_brend');

    Route::post('/admin/brend/uploadSlike', 'adminBrendoviController@upload_slike');

    Route::post('/admin/brend/obrisiUploadSlike', 'adminBrendoviController@obrisi_upload_slike');

    Route::post('/admin/obrisiBrend/{id}', 'adminBrendoviController@obrisi_brend');

    Route::post('/admin/restaurirajBrend/{id}', 'adminBrendoviController@restauriraj_brend');

//--------------KATEGORIJE---------------------
    Route::get('/admin/kategorije', 'adminKategorijeController@kategorije');

    Route::get('/admin/kategorija/{id}', 'adminKategorijeController@kategorija');

    Route::post('/admin/sacuvajKategoriju/{id}', 'adminKategorijeController@sacuvaj_kategoriju');

    Route::post('/admin/kategorija/uploadSlike', 'adminKategorijeController@upload_slike');

    Route::post('/admin/kategorija/obrisiUploadSlike', 'adminKategorijeController@obrisi_upload_slike');

    Route::post('/admin/obrisiKategoriju/{id}', 'adminKategorijeController@obrisi_kategoriju');

    Route::post('/admin/restaurirajKategoriju/{id}', 'adminKategorijeController@restauriraj_kategoriju');

//--------------OPCIJE---------------------
    Route::get('/admin/opcije', 'adminOpcijeController@opcije');

    Route::get('/admin/opcija/{id}', 'adminOpcijeController@opcija');

    Route::post('/admin/sacuvajOpciju/{id}', 'adminOpcijeController@sacuvaj_opciju');

    Route::post('/admin/obrisiOpciju/{id}', 'adminOpcijeController@obrisi_opciju');

    Route::post('/admin/restaurirajOpciju/{id}', 'adminOpcijeController@restauriraj_opciju');

//--------------SPECIFIKACIJE---------------------
    Route::get('/admin/specifikacije', 'adminSpecifikacijeController@specifikacije');

    Route::get('/admin/specifikacija/{id}', 'adminSpecifikacijeController@specifikacija');

    Route::post('/admin/sacuvajSpecifikaciju/{id}', 'adminSpecifikacijeController@sacuvaj_specifikaciju');

    Route::post('/admin/obrisiSpecifikaciju/{id}', 'adminSpecifikacijeController@obrisi_specifikaciju');

    Route::post('/admin/restaurirajSpecifikaciju/{id}', 'adminSpecifikacijeController@restauriraj_specifikaciju');

//--------------TAGOVI---------------------
    Route::get('/admin/tagovi', 'adminTagoviController@tagovi');

    Route::get('/admin/tag/{id}', 'adminTagoviController@tag');

    Route::post('/admin/sacuvajTag/{id}', 'adminTagoviController@sacuvaj_tag');

    Route::post('/admin/obrisiTag/{id}', 'adminTagoviController@obrisi_tag');

    Route::post('/admin/restaurirajTag/{id}', 'adminTagoviController@restauriraj_tag');

//--------------PORUDZBINE---------------------
    Route::get('/admin/porudzbine', 'adminPorudzbineController@porudzbine');

    Route::get('/admin/porudzbina/{id}', 'adminPorudzbineController@porudzbina');

    Route::get('/admin/fakture', 'adminPorudzbineController@fakture');

    Route::get('/admin/faktura/{id}', 'adminPorudzbineController@faktura');

    Route::post('/admin/statusPorudzbine/{id}/{status}', 'adminPorudzbineController@statusPorudzbine');

//--------------KORISNICI---------------------
    Route::get('/admin/korisnici', 'adminKorisniciController@korisnici');

    Route::get('/admin/korisnik/{id}', 'adminKorisniciController@korisnik');

    Route::post('/admin/blokirajKorisnika/{id}', 'adminKorisniciController@blokiraj_korisnika');

    Route::post('/admin/odblokirajKorisnika/{id}', 'adminKorisniciController@odblokiraj_korisnika');

    Route::post('/admin/sacuvajKorisnika/{id}', 'adminKorisniciController@sacuvaj_korisnika');

//--------------IZVESTAJI---------------------
    Route::get('/admin/izvestaji', 'adminIzvestajiController@izvestaji');

    Route::get('/admin/izvestaj', 'adminIzvestajiController@dohvatiIzvestaj');

    Route::post('/admin/izvestajiPost', 'adminIzvestajiController@izvestajiPost');


//--------------KUPONI---------------------
    Route::get('/admin/kuponi', 'adminPopustiController@kuponi');

    Route::get('/admin/kupon/{id}', 'adminPopustiController@kupon');

    Route::post('/admin/sacuvajKupon/{id}', 'adminPopustiController@sacuvaj_kupon');

    Route::post('/admin/obrisiKupon/{id}', 'adminPopustiController@obrisi_kupon');

    Route::post('/admin/restaurirajKupon/{id}', 'adminPopustiController@restauriraj_kupon');

    Route::post('/admin/aktivirajKupon/{id}', 'adminPopustiController@aktiviraj_kupon');

    Route::post('/admin/deaktivirajKupon/{id}', 'adminPopustiController@deaktiviraj_kupon');

//--------------VAUČERI---------------------
    Route::get('/admin/vauceri', 'adminPopustiController@vauceri');

    Route::get('/admin/vaucer/{id}', 'adminPopustiController@vaucer');

    Route::post('/admin/sacuvajVaucer/{id}', 'adminPopustiController@sacuvaj_vaucer');

    Route::post('/admin/obrisiVaucer/{id}', 'adminPopustiController@obrisi_vaucer');

    Route::post('/admin/restaurirajVaucer/{id}', 'adminPopustiController@restauriraj_vaucer');

    Route::post('/admin/iskoristiVaucer/{id}', 'adminPopustiController@iskoristi_vaucer');

//--------------BLOG---------------------
    Route::get('/admin/blog', 'adminBlogController@blog');

    Route::get('/admin/clanak/{id}', 'adminBlogController@clanak');

    Route::post('/admin/sacuvajClanak/{id}', 'adminBlogController@sacuvaj');

    Route::post('/admin/obrisiClanak/{id}', 'adminBlogController@obrisi');

    Route::post('/admin/restaurirajClanak/{id}', 'adminBlogController@restauriraj');

    Route::post('/admin/clanak/uploadSlike', 'adminBlogController@upload_slike');

    Route::post('/admin/clanak/obrisiUploadSlike', 'adminBlogController@obrisi_upload_slike');
});
//---------ADMIN RUTE KRAJ----------------
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/redirect/{provider}', 'Auth\SocialAuthController@redirect');
Route::get('/callback/{provider}', 'Auth\SocialAuthController@callback');
