function prikaziDostupne(){
    $('#tabela-kategorije-obrisane').css('display', 'none');
    $('#kategorije-aktivne').css('display', 'block');
    $('#tabela-kategorije-obrisane_wrapper').css('display', 'none');


    $('#kategorije-title').html('Dostupne kategorije');
}

function prikaziNedostupne(){
    $('#tabela-kategorije-obrisane').css('display', 'block');
    $('#kategorije-aktivne').css('display', 'none');
    $('#tabela-kategorije-obrisane_wrapper').css('display', 'block');

    $('#kategorije-title').html('Obrisane kategorije');
}