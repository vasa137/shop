let $selectableTree;
let categoryTree;

let Category = function(text, tags, nodes) {
    this.text = text;
    this.tags = tags;

    if(nodes != null){
        this.nodes = nodes;
    }
};


var initSelectableTree = function() {
    return $('#treeview-selectable').treeview({
        data: categoryTree,
        multiSelect: true,

        /*
        ako zelimo da kad se selektuje podkategorija da se izbrise selekcija nadkategorija

        onNodeSelected: function(event, node) {
            let parent = $selectableTree.treeview('getParent', node);
            console.log(parent);

            // kada se dohvate najvisi elementi u hijerahiji stabla getParent vraca specificnu vrednost koja ima polje length, dok u drugim slucajevima vraca node koji nema length
            while(parent.length === undefined){
               $selectableTree.treeview('unselectNode', parent);
               parent = $selectableTree.treeview('getParent', parent);
            }
        },
         */
    });
};

// kopira dobijeno stablo kategorija u strukturu pogodnu za bootstrap treeview
function buildCategoryTree(categories, proizvodKategorije = null){
    let branch = [];

    for(let i = 0; i < categories.length; i++){
        let category = categories[i];
        let children;

        if(category['children'] !== null && category['children'].length > 0){
           children  = buildCategoryTree(category['children'], proizvodKategorije);
        } else{
            children = null;
        }

        let newCat = new Category(category['naziv'], category['id'], children);

        let selected = false;
        let expanded = false;

        if(proizvodKategorije != null){
            expanded = true;
            if(proizvodKategorije.indexOf(category['id']) != -1){
                selected = true;
            }
        }

        newCat.state = {
            'selected' : selected,
            'expanded': expanded
        };

        branch.push(newCat);
    }

    return branch;
}

// proizvodKategorije nije null kada se menja proizvod, a jeste kada se dodaje novi
function inicijalizujKategorije(stabloKategorija, proizvodKategorije = null){
    stabloKategorija = JSON.parse(stabloKategorija);

    if(proizvodKategorije != null){
        proizvodKategorije = JSON.parse(proizvodKategorije);
    }

    categoryTree = buildCategoryTree(stabloKategorija, proizvodKategorije);

    $selectableTree = initSelectableTree();
}

