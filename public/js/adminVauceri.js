function prikaziDostupne(){
    $('#tabela-vauceri-obrisani').css('display', 'none');
    $('#tabela-vauceri').css('display', 'block');
    $('#tabela-vauceri-obrisani_wrapper').css('display', 'none');
    $('#tabela-vauceri_wrapper').css('display', 'block');

    $('#vauceri-title').html('Dostupni vaučeri');
}

function prikaziNedostupne(){
    $('#tabela-vauceri-obrisani').css('display', 'block');
    $('#tabela-vauceri').css('display', 'none');
    $('#tabela-vauceri-obrisani_wrapper').css('display', 'block');
    $('#tabela-vauceri_wrapper').css('display', 'none');

    $('#vauceri-title').html('Obrisani vaučeri');
}