var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
Dropzone.autoDiscover = false;
var maxFiles = 100;

function initDropzone(id, maxFiles){
    $('#dropzone-' + id).dropzone({
        url: "/admin/uploadSlike",
        autoProcessQueue: true,
        uploadMultiple: true,
        parallelUploads: 100,
        maxFiles: maxFiles,
        acceptedFiles: "image/*",
        headers: {
            'x-csrf-token': CSRF_TOKEN,
        },
        init: function () {

            var wrapperThis = this;


            this.on("addedfile", function (file) {

                // Create the remove button
                var removeButton = Dropzone.createElement("<button class='btn btn-lg dark offset-2' style='margin-top:2px;'>Obriši</button>");

                // Listen to the click event
                removeButton.addEventListener("click", function (e) {
                    // Make sure the button click doesn't submit the form:
                    //e.preventDefault();
                    //e.stopPropagation();

                    // Remove the file preview.

                    wrapperThis.removeFile(file);
                    // If you want to the delete the file on the server as well,
                    // you can do the AJAX request here.
                    $.ajax({
                        url: "/admin/obrisiUploadSlike",
                        type: "POST",
                        data: {
                            "filename": file.name,
                            "opcija": $('#opcija-dropzone-' +  id).val()
                        }
                    });
                });

                // Add the button to the file preview element.
                file.previewElement.appendChild(removeButton);

                // izbaci postojecu glavnu sliku ukoliko se radi izmena
                if(id == 0 && document.getElementById('slike-opcija-0-glavna.jpg') != null){
                    obrisiSliku('glavna.jpg',0);
                }
            });

            this.on('sendingmultiple', function (data, xhr, formData) {
                formData.append("opcija", $('#opcija-dropzone-' +  id).val());
            });
        }
    });
}


initDropzone('-1', maxFiles);
initDropzone('0', 1);