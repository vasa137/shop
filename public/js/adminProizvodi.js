function prikaziDostupne(){
    $('#obrisani-tabela').css('display', 'none');
    $('#dostupni-tabela').css('display', 'block');
    $('#obrisani-tabela_wrapper').css('display', 'none');
    $('#dostupni-tabela_wrapper').css('display', 'block');

    $('#proizvodi-avail-title').html('Dostupni proizvodi');
}

function prikaziNedostupne(){
    $('#obrisani-tabela').css('display', 'block');
    $('#dostupni-tabela').css('display', 'none');
    $('#obrisani-tabela_wrapper').css('display', 'block');
    $('#dostupni-tabela_wrapper').css('display', 'none');

    $('#proizvodi-avail-title').html('Obrisani proizvodi');
}