<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KuponProizvod extends Model
{
    protected $table = 'kupon_proizvod';

    protected $fillable = ['id_kupon', 'id_proizvod'];

    public static function dohvatiProizvodeZaKupon($id){
        return KuponProizvod::where('id_kupon', $id)->get();
    }

    public static function obrisiProizvodeZaKupon($id){
        KuponProizvod::where('id_kupon', $id)->delete();
    }

    public function napuni($id_kupon, $id_proizvod){
        $this->id_kupon = $id_kupon;
        $this->id_proizvod = $id_proizvod;

        $this->save();
    }
}
