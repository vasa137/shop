<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class Porudzbina extends Model
{
    protected $table = 'porudzbina';

    protected $fillable = ['kupac', 'adresa', 'grad', 'zip', 'email', 'telefon', 'dostava', 'dostava_kupac', 'dostava_adresa', 'dostava_grad' , 'dostava_zip', 'dostava_telefon',
                            'nacin_placanja', 'status', 'placena', 'napomena', 'ima_vaucere', 'id_user', 'firma', 'PIB'];

    protected $appends = ['stavke', 'vauceri', 'user'];

    private $stavke;
    private $vauceri;
    private $user;

    public function setStavkeAttribute($stavke){
        $this->stavke = $stavke;
    }

    public function getStavkeAttribute(){
        return $this->stavke;
    }

    public function setVauceriAttribute($vauceri){
        $this->vauceri = $vauceri;
    }

    public function getVauceriAttribute(){
        return $this->vauceri;
    }

    public function setUserAttribute($user){
        $this->user = $user;
    }

    public function getUserAttribute(){
        return $this->user;
    }

    public static function dohvatiSaId($id){
        return Porudzbina::where('id', $id)->first();
    }

    public static function dohvatiSve(){
        return Porudzbina::all();
    }

    public static function dohvatiSveKojeNisuStornirane(){
        return Porudzbina::where('status', '<>', 'stornirana')->get();
    }

    //dohvata sve porudzbine bez trenutne koja se prikazuje
    public static function dohvatiPrethodnePorudzbineZaKupca($id_user, $id_porudzbina){
        return Porudzbina::where('id_user', $id_user)->where('id', '<>', $id_porudzbina)->orderBy('created_at', 'desc')->get();
    }

    public static function dohvatiZaKupca($id){
        return Porudzbina::where('id_user', $id)->orderBy('created_at', 'desc')->get();
    }

    public function promeniStatus($status){
        $this->status = $status;

        if($this->status == 'kompletirana'){
            $this->placena = 1;
        } else if($this->status == 'stornirana'){
            $this->placena = 0;
        } else if($this->nacin_placanja == 'karticom'){
            $this->placena = 1;
        }

        $this->save();
    }

    public static function dohvatiSaStatusom($status){
        return Porudzbina::where('status',  $status)->orderBy('created_at', 'desc')->get();
    }
    // whereDate da se ne bi racunao ceo timestamp
    public static function dohvatiBrojPorudzbinaZaDatum($datum){
        return count(Porudzbina::whereDate('created_at', "$datum")->get());
    }

    public static function dohvatiBrojPorudzbinaKojeNisuStorniraneUPeriodu($datumOd, $datumDo){
        return count(Porudzbina::where('status', '<>', 'stornirana')->whereDate('created_at', '>=', "$datumOd")->whereDate('created_at', '<', "$datumDo")->get());
    }

    public static function dohvatiBrojPorudzbinaSaStatusomUPeriodu($status, $datumOd, $datumDo){
        return count(Porudzbina::where('status', $status)->whereDate('created_at', '>=', "$datumOd")->whereDate('created_at', '<', "$datumDo")->get());
    }

    public static function dohvatiUkupanIznosZaDatum($datum){
        return DB::select("
            select IFNULL(SUM(p.iznos_popust),0) as suma
            from porudzbina p 
            where p.status <> 'stornirana'
            and DATE(p.created_at) = '$datum'
        ")[0]->suma;
    }


    public static function dohvatiIznoseZaDatume($datumOd, $datumDo){
        return DB::select("
            select IFNULL(SUM(p.iznos_popust),0) as suma, AVG(p.iznos_popust) as prosek, MAX(p.iznos_popust) maksimalna
            from porudzbina p 
            where p.status <> 'stornirana'
            and DATE(p.created_at) >= '$datumOd'
            and DATE(p.created_at) < '$datumDo'
        ")[0];
    }

    public function napuni($kupac, $telefon, $grad, $email, $adresa, $zip, $dostava_kupac, $dostava_telefon, $dostava_grad, $dostava_email, $dostava_adresa, $dostava_zip, $napomena, $id_user){
        $this->kupac = $kupac;
        $this->telefon = $telefon;
        $this->grad = $grad;
        $this->email = $email;
        $this->adresa = $adresa;
        $this->zip = $zip;

        $this->dostava = 1;

        $this->dostava_kupac = $dostava_kupac;
        $this->dostava_telefon = $dostava_telefon;
        $this->dostava_grad = $dostava_grad;
        $this->dostava_email = $dostava_email;
        $this->dostava_adresa = $dostava_adresa;
        $this->dostava_zip = $dostava_zip;

        $this->nacin_placanja = 'pouzecem';

        $this->napomena = $napomena;

        $this->id_user = $id_user;

        $this->save();
    }

}
