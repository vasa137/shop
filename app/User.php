<?php

namespace App;

use App\Notifications\ZaboravljenaLozinkaNotification;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email','password','remember_token','admin', 'blokiran', 'ime_prezime', 'adresa','grad','zip','telefon', 'telefon2','id_vrsta_korisnika', 'admin_napomena'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */

    protected $appends = ['vrsta', 'broj_porudzbina', 'broj_porucenih_proizvoda', 'promet', 'porudzbine'];

    private $vrsta;
    private $broj_porudzbina;
    private $broj_porucenih_proizvoda;
    private $promet;
    private $porudzbine;

    public function setVrstaAttribute($vrsta){
        $this->vrsta = $vrsta;
    }

    public function getVrstaAttribute(){
        return $this->vrsta;
    }

    public function setBrojPorudzbinaAttribute($broj_porudzbina){
        $this->broj_porudzbina = $broj_porudzbina;
    }

    public function getBrojPorudzbinaAttribute(){
        return $this->broj_porudzbina;
    }

    public function setBrojPorucenihProizvodaAttribute($broj_porucenih_proizvoda){
        $this->broj_porucenih_proizvoda = $broj_porucenih_proizvoda;
    }

    public function getBrojPorucenihProizvodaAttribute(){
        return $this->broj_porucenih_proizvoda;
    }

    public function setPrometAttribute($promet){
        $this->promet = $promet;
    }

    public function getPrometAttribute(){
        return $this->promet;
    }

    public function setPorudzbineAttribute($porudzbine){
        $this->porudzbine = $porudzbine;
    }

    public function getPorudzbineAttribute(){
        return $this->porudzbine;
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ZaboravljenaLozinkaNotification($token));
    }

    public static function dohvatiSaEmailom($email){
        return User::where('email', $email)->first();
    }

    public static function dohvatiSveKojiNisuAdmini(){
        return User::where('admin', 0)->get();
    }

    public static function dohvatiSaId($id){
        return User::where('id', $id)->first();
    }

    public static function dohvatiNeblokiraneKojiNisuAdmini(){
        return User::where('admin',0)->where('blokiran', 0)->get();
    }

    public static function dohvatiBlokirane(){
        return User::where('blokiran', 1)->get();
    }

    public function blokiraj(){
        $this->blokiran = 1;
        $this->save();
    }

    public function odblokiraj(){
        $this->blokiran = 0;
        $this->save();
    }

    public function azurirajAdminPodatke($id_vrsta_korisnika, $admin_napomena){
        $this->id_vrsta_korisnika = $id_vrsta_korisnika;
        $this->admin_napomena = $admin_napomena;
        $this->save();
    }

}
