<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class ProizvodKategorija extends Model
{
    protected $table = 'proizvod_kategorija';

    protected $fillable = ['id_proizvod', 'id_kategorija'];

    public function napuni($id_proizvod, $id_kategorija){
        $this->id_proizvod = $id_proizvod;
        $this->id_kategorija = $id_kategorija;

        $this->save();
    }

    public static function dohvatiKategorijeZaProizvod($id){
        return ProizvodKategorija::where('id_proizvod', $id)->get();
    }

    public static function obrisiKategorijeZaProizvod($id){
        ProizvodKategorija::where('id_proizvod', $id)->delete();
    }


    public static function dohvatiBrojRazlicitihProizvodaZaKategoriju($id){
        return DB::select("
            select IFNULL(COUNT(p.id), 0) as broj_proizvoda
            FROM proizvod_kategorija kp, proizvod p 
            WHERE kp.id_proizvod = p.id
            AND kp.id_kategorija = $id
            AND p.sakriven = 0
        ")[0]->broj_proizvoda;
    }

    public static function dohvatiBrojRazlicitihBrendovaZaKategoriju($id){
        return DB::select("
            select IFNULL(COUNT(distinct p.id_brend), 0) as broj_brendova
            FROM proizvod_kategorija pk, proizvod p
            WHERE pk.id_proizvod = p.id
            AND pk.id_kategorija = $id
            AND p.id_brend IS NOT NULL
            AND p.sakriven = 0
        ")[0]->broj_brendova;
    }


}
