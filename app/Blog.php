<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    protected $table = 'blog';

    protected $fillable = ['naslov', 'tekst', 'sakriven','uvod'];

    public static function dohvatiSaId($id){
        return Blog::where('id', $id)->first();
    }


    public function obrisi(){
	    $this->sakriven = 1;
	    $this->save();
    }

    public function restauriraj(){
        $this->sakriven = 0;
        $this->save();
    }

    public static function dohvatiSveAktivne(){
        return Blog::where('sakriven', 0)->get();
    }

    public static function dohvatiSveObrisane(){
        return Blog::where('sakriven', 1)->get();
    }

    public function napuni($naslov, $uvod, $tekst){
        $this->naslov = $naslov;
        $this->uvod = $uvod;
        $this->tekst = $tekst;

        $this->save();
    }
}
