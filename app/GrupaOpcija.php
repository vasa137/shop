<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GrupaOpcija extends Model
{
    protected $table = 'grupa_opcija';

    protected $fillable = ['naziv', 'opis'];

    protected $appends = ['opcije'];

    private $opcije;

    public function setOpcijeAttribute($opcije){
        $this->opcije = $opcije;
    }

    public function getOpcijeAttribute(){
        return $this->opcije;
    }

    public static function dohvatiSve(){
        return GrupaOpcija::all();
    }
}
