<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brend extends Model
{
    protected $table = 'brend';

    protected $fillable = ['naziv', 'opis', 'sakriven'];

    protected $appends = ['broj_proizvoda', 'broj_kategorija'];

    private $broj_proizvoda;
    private $broj_kategorija;

    public function setBrojProizvodaAttribute($broj_proizvoda){
        $this->broj_proizvoda = $broj_proizvoda;
    }

    public function getBrojProizvodaAttribute(){
        return $this->broj_proizvoda;
    }

    public function setBrojKategorijaAttribute($broj_kategorija){
        $this->broj_kategorija = $broj_kategorija;
    }

    public function getBrojKategorijaAttribute(){
        return $this->broj_kategorija;
    }

    public static function dohvatiSveAktivne(){
        return Brend::where('sakriven', 0)->get();
    }

    public static function dohvatiSveObrisane(){
        return Brend::where('sakriven', 1)->get();
    }

    public static function dohvatiSaId($id){
        return Brend::where('id', $id)->first();
    }

    public function napuni($naziv, $opis){
        $this->naziv = $naziv;
        $this->opis = $opis;

        $this->save();
    }

    public function obrisi(){
        $this->sakriven = 1;

        $this->save();
    }

    public function restauriraj(){
        $this->sakriven = 0;

        $this->save();
    }
}
