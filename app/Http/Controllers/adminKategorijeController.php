<?php

namespace App\Http\Controllers;

use App\Kategorija;
use Illuminate\Http\Request;
use File;
use App\ProizvodKategorija;
use App\Utility\Util;
use Redirect;
class adminKategorijeController extends Controller
{

    private function obrisi_temp(){
        $directoryPath = public_path('images/kategorije/temp');
        File::deleteDirectory($directoryPath);

        File::makeDirectory($directoryPath,0755,true);
    }

    private function popuniKategorijaInfo($kategorija){
        $kategorija->broj_proizvoda = ProizvodKategorija::dohvatiBrojRazlicitihProizvodaZaKategoriju($kategorija->id);
        $kategorija->broj_brendova = ProizvodKategorija::dohvatiBrojRazlicitihBrendovaZaKategoriju($kategorija->id);

        $deca = Kategorija::dohvatiNeposrednuDecu($kategorija->id);
        $kategorija->ima_decu = count($deca) == 0 ? false : true;
    }

    public function kategorija($id){
        $this->obrisi_temp();

        $izmena = false;

        if($id > 0){
            $izmena = true;
        }

        $kategorije = Kategorija::dohvatiSveAktivne();
        $stabloKategorija = Util::getInstance()->buildCategoryTree($kategorije, null);

        if(!$izmena){
            return view('admin.adminKategorija', compact('izmena','stabloKategorija'));
        } else{
            $kategorija = Kategorija::dohvatiSaId($id);

            if($kategorija == null){
                abort(404);
            }

            $this->popuniKategorijaInfo($kategorija);

            $kategorija_directory = public_path('images/kategorije/' . $kategorija->id);
            $temp_directory = public_path('images/kategorije/temp');

            if(!File::exists($kategorija_directory)){
                File::makeDirectory($kategorija_directory);
            }

            File::copyDirectory($kategorija_directory, $temp_directory);

            // ZA ADMINA SLIKU VRACAMO KAO GLAVNA.JPG
            if(File::exists($temp_directory . '/' . $kategorija->naziv . '.jpg')) {
                File::move($temp_directory . '/' . $kategorija->naziv . '.jpg', $temp_directory . '/glavna.jpg');
            }

            return view('admin.adminKategorija', compact('izmena', 'kategorija', 'stabloKategorija'));
        }
    }

    public function sacuvaj_kategoriju($id){
        $izmena = false;

        if($id > 0){
            $izmena = true;
        }

        $naziv = $_POST['naziv'];
        $opis = $_POST['opis'];
        $prioritet = $_POST['prioritet'];
        $meta_title = $_POST['meta_title'];
        $meta_desc = $_POST['meta_desc'];

        $id_nad_kategorija = null;

        if(isset($_POST['kategorija'])){
            $id_nad_kategorija = $_POST['kategorija'];
        }

        $zaPunjenje = true;

        if($izmena){
            $kategorija = Kategorija::dohvatiSaId($id);

            if($kategorija->naziv == $naziv && $kategorija->prioritet == $prioritet && $kategorija->opis ==  $opis && $kategorija->id_nad_kategorija == $id_nad_kategorija && $kategorija->meta_title == $meta_title && $kategorija->meta_desc == $meta_desc){
                $zaPunjenje = false;
            }

        } else{
            $kategorija = new Kategorija();
        }

        if($zaPunjenje) {
            $nivo = 0;

            if($id_nad_kategorija != null) {
                $nad_kategorija = Kategorija::dohvatiSaId($id_nad_kategorija);
                $nivo = $nad_kategorija->nivo + 1;
            }


            $kategorija->napuni($naziv, $opis, $nivo, $prioritet, $id_nad_kategorija, $meta_title, $meta_desc);
        }

        $kategorija_directory = public_path('images/kategorije/' . $kategorija->id);
        $temp_directory = public_path('images/kategorije/temp');

        if(File::exists($kategorija_directory)){
            File::deleteDirectory($kategorija_directory);
        }

        File::makeDirectory($kategorija_directory,0755,true);

        File::copyDirectory($temp_directory, $kategorija_directory);

        // PROMENA NAZIVA GLAVNE SLIKE ZBOG OPTIMIZACIJE
        if(File::exists($kategorija_directory . '/glavna.jpg')) {
            File::move($kategorija_directory . '/glavna.jpg', $kategorija_directory . '/' . $kategorija->naziv . '.jpg');
        }

        return redirect('/admin/kategorija/' . $kategorija->id);
    }

    public function kategorije(){
        $aktivneKategorije = Kategorija::dohvatiAktivneKategorijeSortiranePoPrioritetu();
        $stabloKategorija = Util::getInstance()->buildCategoryTree($aktivneKategorije, null);

        $obrisaneKategorije = Kategorija::dohvatiSveObrisane();

        foreach($aktivneKategorije as $kategorija){
            $this->popuniKategorijaInfo($kategorija);
        }

        foreach($obrisaneKategorije as $kategorija){
            $this->popuniKategorijaInfo($kategorija);
        }

        return view('admin.adminKategorije', compact('aktivneKategorije', 'stabloKategorija', 'obrisaneKategorije'));
    }

    public function obrisi_kategoriju($id){
        $kategorija = Kategorija::dohvatiSaId($id);

        $kategorija->obrisi();

        return Redirect::back();
    }

    public function restauriraj_kategoriju($id){
        $kategorija = Kategorija::dohvatiSaId($id);

        $kategorija->restauriraj();

        return Redirect::back();
    }

    public function upload_slike(){
        $image = $_FILES['file'];

        $directoryPath = public_path('images/kategorije/temp');
        $image_name = 'glavna.jpg';

        //$filename = time() . '.' . pathinfo('' . $image['name'][0], PATHINFO_EXTENSION);

        File::move($image['tmp_name'][0], $directoryPath . '/' . $image_name);

        chmod($directoryPath . '/' .$image_name, 0644);
    }

    public function obrisi_upload_slike(){
        $image_name = $_POST['filename'];

        $directoryPath = public_path('images/kategorije/temp');

        File::delete($directoryPath . '/' . $image_name);
    }
}
