<?php

namespace App\Http\Controllers;

use App\Kupon;
use App\Opcija;
use App\Porudzbina;
use App\PorudzbinaVaucer;
use App\Proizvod;
use App\ProizvodOpcija;
use App\StavkaOpcija;
use App\StavkaPorudzbina;
use App\User;
use App\Vaucer;
use App\VrstaKorisnika;
use Illuminate\Http\Request;
use Redirect;
class adminPorudzbineController extends Controller
{

    private function pokupiPorudzbinu($id){
        $porudzbina = Porudzbina::dohvatiSaId($id);

        if($porudzbina == null){
            abort(404);
        }

        $porudzbina->stavke = StavkaPorudzbina::dohvatiStavkeZaPorudzbinu($id);

        if($porudzbina->ima_vaucere){
            $porudzbinaVauceri = PorudzbinaVaucer::dohvatiVaucereZaPorudzbinu($id);

            $vauceri = [];

            foreach($porudzbinaVauceri as $porudzbinaVaucer){
                $vauceri [] = Vaucer::dohvatiSaId($porudzbinaVaucer->id_vaucer);
            }

            $porudzbina->vauceri = $vauceri;
        }

        if($porudzbina->id_user != null){
            $porudzbina->user = User::dohvatiSaId($porudzbina->id_user);

            if($porudzbina->user->id_vrsta_korisnika != null) {
                $porudzbina->user->vrsta = VrstaKorisnika::dohvatiSaId($porudzbina->user->id_vrsta_korisnika);
            }
        }

        foreach($porudzbina->stavke as $stavka){
            $stavka->proizvod = Proizvod::dohvatiSaId($stavka->id_proizvod);

            $stavkeOpcije = StavkaOpcija::dohvatiOpcijeZaStavku($stavka->id);

            $nizOpcija = [];

            foreach($stavkeOpcije as $stavkaOpcija){
                $nizOpcija [] = Opcija::dohvatiSaId($stavkaOpcija->id_opcija);
            }

            $stavka->opcije = $nizOpcija;

            if($stavka->id_kupon != null){
                $stavka->kupon = Kupon::dohvatiSaId($stavka->id_kupon);
            }
        }

        return $porudzbina;
    }

	public function porudzbina($id)
    {
        $porudzbina = $this->pokupiPorudzbinu($id);

        $brojProizvodaKorpa = 0;

        foreach($porudzbina->stavke as $stavka) {
            $brojProizvodaKorpa += $stavka->kolicina;
        }

    	// DODATNO ZA KUPCA
        $kupacPorudzbine = null;
        $brojPorudzbina = 0;
        $brojProizvoda = 0;
        $iznosPorudzbina = 0;

        if($porudzbina->id_user != null) {
            $kupacPorudzbine = Porudzbina::dohvatiPrethodnePorudzbineZaKupca($porudzbina->id_user, $porudzbina->id);

            foreach($kupacPorudzbine as $kupacPorudzbina){
                if($kupacPorudzbina != 'stornirana'){
                    $brojPorudzbina++;
                    $iznosPorudzbina += $kupacPorudzbina->iznos_popust;
                    $brojProizvoda += StavkaPorudzbina::dohvatiBrojProizvodaZaPorudzbinu($kupacPorudzbina->id);
                }
            }

            $porudzbina->user->broj_porudzbina = $brojPorudzbina;
            $porudzbina->user->broj_porucenih_proizvoda = $brojProizvoda;
            $porudzbina->user->promet = $iznosPorudzbina;
        }

    	return view('admin.adminPorudzbina', compact('porudzbina', 'brojProizvodaKorpa', 'kupacPorudzbine'));
    }


    public function porudzbine()
    {
    	$porudzbine = Porudzbina::dohvatiSve();

    	$brojNovih = 0;
    	$brojPoslatih = 0;
    	$brojKompletiranih = 0;
    	$brojStorniranih = 0;
        $ovogMeseca = 0;
        $prometMeseca = 0;

    	foreach($porudzbine as $porudzbina){
    	    switch($porudzbina->status){
                case 'nova':
                    $brojNovih++;
                    break;
                case 'poslata':
                    $brojPoslatih++;
                    break;
                case 'stornirana':
                    $brojStorniranih++;
                    break;
                case 'kompletirana':
                    $brojKompletiranih++;
                    break;
            }

            if(date("m",strtotime($porudzbina->created_at) == date("m", time()))){
                $ovogMeseca++;
                if($porudzbina->status == 'kompletirana') {
                    $prometMeseca += $porudzbina->iznos_popust;
                }
            }

        }

    	return view('admin.adminPorudzbine', compact('porudzbine', 'brojNovih', 'brojPoslatih', 'brojKompletiranih', 'brojStorniranih', 'ovogMeseca', 'prometMeseca'));
    }

    public function statusPorudzbine($id, $status)
    {
        $porudzbina = Porudzbina::dohvatiSaId($id);

        if ($porudzbina->status != $status) {
            $stavke = StavkaPorudzbina::dohvatiStavkeZaPorudzbinu($id);

            // AKO JE PORUDZBINA BILA STORNIRANA, A SAD PRELAZI U NEKO DRUGO STANJE, ILI JE BILA U NEKOM DRUGOM STANJU A SAD SE STORNIRA
            if($porudzbina->status == 'stornirana' || $status == 'stornirana') {
                foreach ($stavke as $stavka) {
                    if ($stavka->ima_opcije) {
                        $stavkaOpcije = StavkaOpcija::dohvatiOpcijeZaStavku($stavka->id);

                        foreach ($stavkaOpcije as $stavkaOpcija) {
                            $proizvodOpcija = ProizvodOpcija::dohvatiProizvodOpciju($stavka->id_proizvod, $stavkaOpcija->id_opcija);

                            // MOZDA OPCIJA VISE NIJE AKTUELNA
                            if ($proizvodOpcija != null) {
                                // AKO JE SAD STORNIRANA, VRATI OPCIJE NA STANJE
                                if($status == 'stornirana'){
                                    $opcija_broj_komada = $proizvodOpcija->br_komada + $stavka->kolicina;
                                }
                                // U SUPROTNOM SKINI SA STANJA
                                else{
                                    $opcija_broj_komada = $proizvodOpcija->br_komada - $stavka->kolicina;

                                    /*
                                    // AKO SE NE PRATI BROJ KOMADA NE DAJ DA ODE U MINUS
                                    if($proizvodOpcija->lager != 'prati_broj_komada'){
                                        if($opcija_broj_komada < 0){
                                            $opcija_broj_komada = 0;
                                        }
                                    }
                                    */
                                }

                                $proizvodOpcija->azurirajBrojKomada($opcija_broj_komada);

                            }
                        }
                    }

                    $proizvod = Proizvod::dohvatiSaId($stavka->id_proizvod);

                    if($status == 'stornirana'){
                        $broj_komada = $proizvod->br_komada + $stavka->kolicina;
                    } else{
                        $broj_komada = $proizvod->br_komada - $stavka->kolicina;
                    }

                    /*
                    if($proizvod->lager != 'prati_broj_komada'){
                        if($broj_komada < 0){
                            $broj_komada = 0;
                        }
                    }
                    */


                    $proizvod->azurirajBrojKomada($broj_komada);

                }
            }

            $porudzbina->promeniStatus($status);
        }

        return Redirect::back();
    }


    public function fakture()
    {
    	$porudzbine = Porudzbina::dohvatiSveKojeNisuStornirane();

    	return view('admin.adminFakture', compact('porudzbine'));
    }


    public function faktura($id)
    {
        $porudzbina = $this->pokupiPorudzbinu($id);

        return view('admin.adminFaktura', compact('porudzbina'));
    }

}
