<?php

namespace App\Http\Controllers;

use App\Brend;
use App\Dobavljac;
use App\GrupaOpcija;
use App\Kategorija;
use App\Opcija;
use App\OpisProizvoda;
use App\Proizvod;
use App\ProizvodKategorija;
use App\ProizvodOpcija;
use App\ProizvodOpisProizvoda;
use App\ProizvodSpecifikacija;
use App\Specifikacija;
use App\ProizvodTag;
use App\StavkaOpcija;
use App\StavkaPorudzbina;
use App\Tag;
use App\Utility\Util;
use Illuminate\Http\Request;
use File;
use Redirect;
class adminProizvodiController extends Controller
{

    private function pokupiNizFajlova($direktorijum)
    {
        $fajlovi = [];
        foreach (scandir($direktorijum) as $file) {
            if ('.' === $file) continue;
            if ('..' === $file) continue;

            $fajlovi[] = $file;
        }

        return $fajlovi;
    }

    public function proizvod($id)
    {
        $this->obrisi_temp();

        $izmena = false;

        $brendovi = Brend::dohvatiSveAktivne();
        $dobavljaci = Dobavljac::dohvatiSve();

        $kategorije = Kategorija::dohvatiAktivneKategorijeSortiranePoPrioritetu();
        $stabloKategorija = Util::getInstance()->buildCategoryTree($kategorije, null);

        $specifikacije = Specifikacija::dohvatiSveAktivne();
        $grupeOpcija = GrupaOpcija::dohvatiSve();

        $opcije = Opcija::dohvatiSveAktivne();

        // razvrstaj opcije u nizove odredjenih grupa
        foreach ($opcije as $opcija) {
            $opcijePoGrupama[$opcija->id_grupa_opcija][] = $opcija;
        }

        $opisiProizvoda = OpisProizvoda::dohvatiSve();

        $tagovi = Tag::dohvatiSveAktivne();

        if ($id == -1) {
            return view('admin.adminProizvod', compact('izmena', 'brendovi', 'dobavljaci', 'stabloKategorija', 'specifikacije', 'grupeOpcija', 'opcijePoGrupama', 'opisiProizvoda', 'tagovi'));
        } else {
            //

            $proizvod = Proizvod::dohvatiSaId($id);

            if ($proizvod == null) {
                abort(404);
            }

            $izmena = true; // fleg da proizvod vec postoji

            if ($proizvod->id_brend != null) {
                $proizvod->brend = Brend::dohvatiSaId($proizvod->id_brend);
            }

            if ($proizvod->id_dobavljac != null) {
                $proizvod->dobavljac = Dobavljac::dohvatiSaId($proizvod->id_dobavljac);
            }

            $proizvodNizKategorija = [];
            $proizvodKategorije = ProizvodKategorija::dohvatiKategorijeZaProizvod($proizvod->id);
            foreach ($proizvodKategorije as $proizvodKategorija) {
                $proizvodNizKategorija[] = $proizvodKategorija->id_kategorija;
            }

            $proizvod->kategorije = $proizvodNizKategorija;

            if ($proizvod->ima_specifikacije) {
                $proizvodNizSpecifikacija = [];
                $proizvodNizSpecifikacijaTekst = [];
                $proizvodSpecifikacije = ProizvodSpecifikacija::dohvatiSpecifikacijeZaProizvod($proizvod->id);

                foreach ($proizvodSpecifikacije as $proizvodSpecifikacija) {
                    $proizvodNizSpecifikacija[] = $proizvodSpecifikacija->id_specifikacija;
                    $proizvodNizSpecifikacijaTekst[] = $proizvodSpecifikacija->tekst;
                }

                $proizvod->specifikacije = $proizvodNizSpecifikacija;
                $proizvod->specifikacije_tekst = $proizvodNizSpecifikacijaTekst;
            }

            if ($proizvod->ima_opcije) {
                $proizvod->opcije = ProizvodOpcija::dohvatiOpcijeZaProizvod($proizvod->id);

                $proizvodNizGrupeOpcija = [];
                $proizvodNizOpcija = [];

                foreach ($proizvod->opcije as $proizvodOpcija) {

                    $opcija = Opcija::dohvatiSaId($proizvodOpcija->id_opcija);

                    if (!in_array($opcija->id_grupa_opcija, $proizvodNizGrupeOpcija)) {
                        $proizvodNizGrupeOpcija[] = $opcija->id_grupa_opcija;
                    }

                    $proizvodOpcija->grupa_opcija = $opcija->id_grupa_opcija;
                    $proizvodNizOpcija[] = $opcija->id;
                }

                $proizvod->grupe_opcija = $proizvodNizGrupeOpcija;

                $proizvod->niz_opcija = $proizvodNizOpcija;
            }

            if ($proizvod->ima_tagove) {
                $proizvodNizTagova = [];
                $proizvodTagovi = ProizvodTag::dohvatiTagoveZaProizvod($proizvod->id);

                foreach ($proizvodTagovi as $proizvodTag) {
                    $proizvodNizTagova[] = $proizvodTag->id_tag;
                }

                $proizvod->tagovi = $proizvodNizTagova;
            }

            $proizvodOpisProizvodi = ProizvodOpisProizvoda::dohvatiOpiseZaProizvod($proizvod->id);
            $proizvodMapaOpisaProizvoda = [];

            foreach ($opisiProizvoda as $opis) {
                $proizvodMapaOpisaProizvoda[$opis->id] = false;
            }

            foreach ($proizvodOpisProizvodi as $proizvodOpisProizvod) {
                $proizvodMapaOpisaProizvoda[$proizvodOpisProizvod->id_opis_proizvoda] = true;
            }

            $proizvod->opisi = $proizvodMapaOpisaProizvoda;

            $proizvod->broj_porucivanja = StavkaPorudzbina::dohvatiBrojNarucenihProizvodaSaId($id);

            $proizvod_directory = public_path('images/proizvodi/' . $proizvod->id);
            $temp_directory = public_path('images/proizvodi/temp');

            if(!File::exists($proizvod_directory)){
                File::makeDirectory($proizvod_directory);
                File::makeDirectory($proizvod_directory . '/glavna');
                File::makeDirectory($proizvod_directory. '/sveSlike');
            }

            File::copyDirectory($proizvod_directory . '/glavna', $temp_directory . '/glavna');
            File::copyDirectory($proizvod_directory . '/sveSlike', $temp_directory . '/sveSlike');

            // ******VRACANJE NAZIVA GLAVNE SLIKE ZA ADMIN PANEL
            $proizvod_naziv_glavne_slike = Util::getInstance()->nazivGlavneSlike($proizvod);

            if(File::exists($temp_directory . '/glavna/' . $proizvod_naziv_glavne_slike . '.jpg')) {
                File::move($temp_directory . '/glavna/' . $proizvod_naziv_glavne_slike . '.jpg', $temp_directory . '/glavna/glavna.jpg');
            }
            // ***************************************************

            $proizvod->sveSlike = Util::getInstance()->pokupiNizFajlova($temp_directory . '/sveSlike');

            if ($proizvod->ima_opcije) {
                $mapaOpcijeSlike = [];

                foreach ($proizvod->opcije as $proizvodOpcija) {
                    $idOpcije = $proizvodOpcija->id_opcija;

                    $mapaOpcijeSlike[$idOpcije] = [];

                    if ($proizvodOpcija->ima_slike) {


                        File::makeDirectory($temp_directory . '/' . $idOpcije,0755,true);
                        File::copyDirectory($proizvod_directory . '/' . $idOpcije, $temp_directory . '/' . $idOpcije);

                        $mapaOpcijeSlike[$idOpcije] = Util::getInstance()->pokupiNizFajlova($temp_directory . '/' . $idOpcije);
                    }
                }

                $proizvod->opcijeSlike = $mapaOpcijeSlike;
            }

            return view('admin.adminProizvod', compact('izmena', 'proizvod', 'brendovi', 'dobavljaci', 'stabloKategorija', 'specifikacije', 'grupeOpcija', 'opcijePoGrupama', 'opisiProizvoda', 'tagovi'));
        }

    }

    private function popuniProizvodeZaPrikazSvih($proizvodi)
    {
        foreach ($proizvodi as $proizvod) {
            if ($proizvod->id_brend == null) {
                $proizvod->brend = "";
            } else {
                $proizvod->brend = Brend::dohvatiSaId($proizvod->id_brend)->naziv;
            }

            $proizvodOpisi = ProizvodOpisProizvoda::dohvatiOpiseZaProizvod($proizvod->id);

            $opisi = [];

            foreach ($proizvodOpisi as $proizvodOpis) {
                $opisi [] = OpisProizvoda::dohvatiSaId($proizvodOpis->id_opis_proizvoda);
            }

            $proizvod->opisi = $opisi;
        }



    }

    public function proizvodi()
    {
    	$proizvodi = Proizvod::dohvatiSveAktivne();
    	$obrisaniProizvodi = Proizvod::dohvatiSveObrisane();

    	$this->popuniProizvodeZaPrikazSvih($proizvodi);
    	$this->popuniProizvodeZaPrikazSvih($obrisaniProizvodi);

    	$brojNaAkciji = 0;

    	foreach($proizvodi as $proizvod){
    	    if($proizvod->na_popustu){
    	        $brojNaAkciji++;
            }
        }

    	return view('admin.adminProizvodi', compact('proizvodi', 'obrisaniProizvodi', 'brojNaAkciji'));
    }


    public function sacuvaj_proizvod($id)
    {
        $izmena = true;

        if($id == -1){
            $izmena = false;
        }

        $naziv = $_POST['naziv'];
        $sifra = $_POST['sifra'];
        $brend = $_POST['brend'];

        if($brend == ''){
            $brend = null;
        }

        $dobavljac = $_POST['dobavljac'];
        if($dobavljac == ''){
            $dobavljac = null;
        }

        $opis = $_POST['opis'];
        $kratak_opis = $_POST['kratak_opis'];


        $ima_specifikacije = false;

        if(isset($_POST['specifikacije'])){
            $specifikacije = $_POST['specifikacije'];
            $specifikacijeTekst = $_POST['specifikacijeTekst'];
            $ima_specifikacije = true;
        }

        $ima_opcije = false;

        if(isset($_POST['opcije'])){
            $opcije = $_POST['opcije'];
            $ima_opcije = true;
        }

        $ima_tagove = false;

        if(isset($_POST['tagovi'])){
            $tagovi = $_POST['tagovi'];
            $ima_tagove = true;
        }

        $nabavna_cena = $_POST['nabavna_cena'];
        $cena = $_POST['cena'];

        $na_popustu = 0;
        if(isset($_POST['na_popustu'])){
            $na_popustu = $_POST['na_popustu'];
        }

        $cena_popust = $_POST['cena_popust'];
        $lager = $_POST['lager'];
        $br_komada = $_POST['br_komada'];

        $meta_title = $_POST['meta_title'];
        $meta_tags = $_POST['meta_tags'];
        $meta_desc = $_POST['meta_desc'];

        if($izmena){
            $proizvod = Proizvod::dohvatiSaId($id);
        } else{
            $proizvod = new Proizvod();
        }

        $proizvod->napuni($sifra, $naziv, $opis, $kratak_opis, $cena, $na_popustu, $cena_popust, $ima_opcije,
            $lager, $br_komada, $brend, $dobavljac, $nabavna_cena, $ima_specifikacije, $ima_tagove, $meta_title, $meta_tags, $meta_desc);

        if($izmena){
            ProizvodSpecifikacija::obrisiSpecifikacijeZaProizvod($id);
        }

        if($ima_specifikacije) {
             for ($i = 0; $i < count($specifikacije); $i++) {
                $proizvodSpecifikacija = new ProizvodSpecifikacija();
                $proizvodSpecifikacija->napuni($proizvod->id, $specifikacije[$i], $specifikacijeTekst[$i]);
            }
        }

        if($izmena){
            ProizvodOpcija::obrisiOpcijeZaProizvod($id);
        }

        if($ima_opcije){
            foreach($opcije as $idOpcije){
                $opcija_sifra = $_POST['opcija-' . $idOpcije . '-sifra'];
                $opcija_cena = $_POST['opcija-' . $idOpcije . '-cena'];

                $opcija_na_popustu = 0;
                if(isset($_POST['opcija-' . $idOpcije . '-na_popustu'])) {
                    $opcija_na_popustu = $_POST['opcija-' . $idOpcije . '-na_popustu'];
                }

                $opcija_cena_popust = $_POST['opcija-' . $idOpcije . '-cena_popust'];
                $opcija_lager = $_POST['opcija-' . $idOpcije . '-lager'];
                $opcija_br_komada = $_POST['opcija-' . $idOpcije . '-br_komada'];
                $opcija_nabavna_cena = $_POST['opcija-' . $idOpcije . '-nabavna_cena'];

                $opcija_ima_slike = 0;
                if(isset($_POST['opcija-' . $idOpcije . '-ima_slike'])) {
                    $opcija_ima_slike = $_POST['opcija-' . $idOpcije . '-ima_slike'];
                }

                $proizvodOpcija = new ProizvodOpcija();
                $proizvodOpcija->napuni($proizvod->id, $idOpcije, $opcija_sifra, $opcija_cena, $opcija_na_popustu, $opcija_cena_popust, $opcija_lager, $opcija_br_komada, $opcija_nabavna_cena, $opcija_ima_slike);
            }
        }

        if($izmena){
            ProizvodTag::obrisiTagoveZaProizvod($id);
        }

        if($ima_tagove){
            foreach($tagovi as $idTag){
                $proizvodTag = new ProizvodTag();
                $proizvodTag->napuni($proizvod->id, $idTag);
            }
        }

        if($izmena){
            ProizvodKategorija::obrisiKategorijeZaProizvod($id);
        }

        if(isset($_POST['kategorije'])) {
            $kategorije = $_POST['kategorije'];

            foreach($kategorije as $idKategorije){
                $proizvodKategorija = new ProizvodKategorija();
                $proizvodKategorija->napuni($proizvod->id, $idKategorije);
            }
        }

        if($izmena){
            ProizvodOpisProizvoda::obrisiOpiseZaProizvod($id);
        }

        if(isset($_POST['opisiProizvoda'])){
            $opisiProizvoda = $_POST['opisiProizvoda'];

            foreach($opisiProizvoda as $idOpis){
                $proizvodOpisProizvoda = new ProizvodOpisProizvoda();
                $proizvodOpisProizvoda->napuni($proizvod->id, $idOpis);
            }
        }

        $proizvod_directory = public_path('images/proizvodi/' . $proizvod->id);
        $temp_directory = public_path('images/proizvodi/temp');

        if(File::exists($proizvod_directory)){
            File::deleteDirectory($proizvod_directory);
        }

        File::makeDirectory($proizvod_directory,0755,true);
        File::makeDirectory($proizvod_directory . '/glavna',0755,true);
        File::makeDirectory($proizvod_directory . '/sveSlike',0755,true);

        File::copyDirectory($temp_directory . '/glavna', $proizvod_directory . '/glavna');
        File::copyDirectory($temp_directory . '/sveSlike', $proizvod_directory . '/sveSlike');


        // ******PROMENA NAZIVA GLAVNE SLIKE ZBOG OPTIMIZACIJE
        $proizvod_naziv_glavne_slike = Util::getInstance()->nazivGlavneSlike($proizvod);

        if(File::exists($proizvod_directory . '/glavna/glavna.jpg')) {
            File::move($proizvod_directory . '/glavna/glavna.jpg', $proizvod_directory . '/glavna/' . $proizvod_naziv_glavne_slike . '.jpg');
        }
        // ***************************************************


        if($ima_opcije) {
            foreach ($opcije as $idOpcije) {
                if(isset($_POST['opcija-' . $idOpcije . '-ima_slike'])) {
                    File::makeDirectory($proizvod_directory . '/' . $idOpcije,0755,true);
                    File::copyDirectory($temp_directory . '/' . $idOpcije, $proizvod_directory . '/' . $idOpcije);
                }
            }
        }

        $this->obrisi_temp();

        return redirect('/admin/proizvod/' . $proizvod->id);
    }


    private function obrisi_temp(){
        $directoryPath = public_path('images/proizvodi/temp');
        File::deleteDirectory($directoryPath);

        File::makeDirectory($directoryPath,0755,true);
        File::makeDirectory($directoryPath . '/sveSlike',0755,true);
        File::makeDirectory($directoryPath . '/glavna',0755,true);
    }
    //u php.ini upload max filesize!!!

    // ne brisati
    public function upload_slike(Request $request){
        $opcija = $_POST['opcija'];
        $images = $_FILES['file'];

        if($opcija == -1) {
            $directoryPath = public_path('images/proizvodi/temp/sveSlike');
        } else if($opcija == 0) {
            $directoryPath = public_path('images/proizvodi/temp/glavna');
            $image_name = 'glavna.jpg';
            File::deleteDirectory($directoryPath);
            File::makeDirectory($directoryPath,0755,true);
        }else{
            $directoryPath = public_path('images/proizvodi/temp' . '/' . $opcija);

            if(!File::exists($directoryPath)) {
                File::makeDirectory($directoryPath,0755,true);
            }
        }

        if($opcija == 0){
            //$filename = time() . '.' . pathinfo('' . $image['name'][0], PATHINFO_EXTENSION);
            File::move($images['tmp_name'][0], $directoryPath . '/' . $image_name);
            chmod($directoryPath . '/' .$image_name, 0644);
        } else{
            for($i = 0; $i < count($images['name']); $i++){
                $image_name = $images['name'][$i];
                //$filename = time() . '.' . pathinfo('' . $image['name'][0], PATHINFO_EXTENSION);
                File::move($images['tmp_name'][$i], $directoryPath . '/' . $image_name);
                chmod($directoryPath . '/' .$image_name, 0644);
            }
        }
    }

    public function obrisi_upload_slike(){
        $opcija = $_POST['opcija'];
        $filename = $_POST['filename'];

        $image_name = $filename;

        if($opcija == -1) {
            $directoryPath = public_path('images/proizvodi/temp/sveSlike');
        } else if($opcija == 0) {
            $directoryPath = public_path('images/proizvodi/temp/glavna');
            $image_name = 'glavna.jpg';
        } else{
            $directoryPath = public_path('images/proizvodi/temp' . '/' . $opcija);
        }

        File::delete($directoryPath . '/' . $image_name);

    }

    public function obrisi_proizvod($id)
    {
    	$proizvod = Proizvod::dohvatiSaId($id);

    	$proizvod->obrisi();

    	return Redirect::back();
    }

    public function restauriraj_proizvod($id)
    {
        $proizvod = Proizvod::dohvatiSaId($id);

        $proizvod->restauriraj();

        return Redirect::back();
    }


}
