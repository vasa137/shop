<?php

namespace App\Http\Controllers;

use App\Porudzbina;
use App\Proizvod;
use App\StavkaPorudzbina;
use Illuminate\Http\Request;

class adminController extends Controller
{
    public function naslovna()
    {
    	$novePorudzbine = Porudzbina::dohvatiSaStatusom('nova');

        $datum =  date('Y-m-d', time());
        $datum = date('Y-m-d', strtotime($datum . ' -6 day'));

        $datum1 = $datum;

        $brojeviPorudzbina = [];
        $iznosi = [];
        $datumi = [];

        for($i = 0; $i < 7; $i++){
            $datumi[] = date('d.m.Y.', strtotime($datum));

            $brojeviPorudzbina[] = Porudzbina::dohvatiBrojPorudzbinaZaDatum($datum);
            $iznosi[] = Porudzbina::dohvatiUkupanIznosZaDatum($datum);

            $datum =  date('Y-m-d', strtotime($datum . ' +1 day'));
        }

        $datum2 = $datum;

        $brojNestorniranih = Porudzbina::dohvatiBrojPorudzbinaKojeNisuStorniraneUPeriodu($datum1, $datum2);
        $brojStorniranih = Porudzbina::dohvatiBrojPorudzbinaSaStatusomUPeriodu('stornirana', $datum1, $datum2);

        $najprodavanijiNiz = StavkaPorudzbina::dohvatiNajprodavanijeProizvode();
        $najprodavanijiProizvodi = [];

        foreach($najprodavanijiNiz as $info){
            $proizvod = Proizvod::dohvatiSaId($info->id_proizvod);
            $proizvod->broj_porucivanja = $info->broj_prodatih;
            $najprodavanijiProizvodi [] = $proizvod;
        }

        $infoIznosi = Porudzbina::dohvatiIznoseZaDatume($datum1, $datum2);

        $ukupanIznos = $infoIznosi->suma;
        $prosecanIznos = $infoIznosi->prosek;
        $maksimalanIznos = $infoIznosi->maksimalna;

    	return view('admin.adminWelcome', compact('novePorudzbine', 'datumi', 'brojeviPorudzbina', 'brojNestorniranih', 'brojStorniranih', 'najprodavanijiProizvodi', 'iznosi', 'ukupanIznos', 'prosecanIznos', 'maksimalanIznos'));
    }



}
