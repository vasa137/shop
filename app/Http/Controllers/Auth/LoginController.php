<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use App\User;
use Illuminate\Support\Facades\Hash;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    // OVERRIDE za blokiranog korisnika
    protected function credentials(Request $request)
    {
        return array_merge($request->only($this->username(), 'password'), ['blokiran' => 0]);
    }

    // OVERRIDE ZA ERROR ZA BLOKIRANOG KORISNIKA
    protected function sendFailedLoginResponse(Request $request)
    {
        $user = User::dohvatiSaEmailom($request->{$this->username()});

        $blocked = false;

        if($user != null){
            if(Hash::check( $request->password, $user->password)) {
                $blocked = true;
            }
        }

        if($blocked){
            throw ValidationException::withMessages([
                $this->username() => [trans('auth.blocked')],
            ]);
        } else {
            throw ValidationException::withMessages([
                $this->username() => [trans('auth.failed')],
            ]);
        }
    }
}
