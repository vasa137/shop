<?php

namespace App\Http\Controllers;

use App\ProizvodSpecifikacija;
use App\Specifikacija;
use Illuminate\Http\Request;
use Redirect;
class adminSpecifikacijeController extends Controller
{

    private function popuniSpecifikacijaInfo($specifikacija){
        $specifikacija->broj_proizvoda = ProizvodSpecifikacija::dohvatiBrojProizvodaZaSpecifikaciju($specifikacija->id);
    }

    public function specifikacija($id){
        $izmena = false;

        if($id > 0){
            $izmena = true;
        }

        if(!$izmena){
            return view('admin.adminSpecifikacija', compact('izmena'));
        } else{
            $specifikacija = Specifikacija::dohvatiSaId($id);

            if($specifikacija == null){
                abort(404);
            }

            $this->popuniSpecifikacijaInfo($specifikacija);

            return view('admin.adminSpecifikacija', compact('izmena', 'specifikacija'));
        }
    }

    public function specifikacije(){
        $aktivneSpecifikacije = Specifikacija::dohvatiSveAktivne();
        $obrisaneSpecifikacije = Specifikacija::dohvatiSveObrisane();

        foreach($aktivneSpecifikacije as $specifikacija){
            $this->popuniSpecifikacijaInfo($specifikacija);
        }

        foreach($obrisaneSpecifikacije as $specifikacija){
            $this->popuniSpecifikacijaInfo($specifikacija);
        }

        return view('admin.adminSpecifikacije', compact('aktivneSpecifikacije', 'obrisaneSpecifikacije'));
    }

    public function sacuvaj_specifikaciju($id){
        $izmena = false;

        if($id > 0){
            $izmena = true;
        }

        $naziv = $_POST['naziv'];
        $opis = $_POST['opis'];

        $zaPunjenje = true;

        if($izmena){
            $specifikacija = Specifikacija::dohvatiSaId($id);

            if($specifikacija->naziv == $naziv && $specifikacija->opis == $opis ){
                $zaPunjenje = false;
            }

        } else{
            $specifikacija = new Specifikacija();
        }

        if($zaPunjenje) {
            $specifikacija->napuni($naziv, $opis);
        }

        return redirect('/admin/specifikacija/' . $specifikacija->id);
    }

    public function obrisi_specifikaciju($id){
        $specifikacija = Specifikacija::dohvatiSaId($id);

        $specifikacija->obrisi();

        return Redirect::back();
    }

    public function restauriraj_specifikaciju($id){
        $specifikacija = Specifikacija::dohvatiSaId($id);

        $specifikacija->restauriraj();

        return Redirect::back();
    }

}
